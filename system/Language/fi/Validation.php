<?php

/**
 * Validation language strings.
 *
 * @package    CodeIgniter
 * @author     CodeIgniter Dev Team
 * @copyright  2019-2020 CodeIgniter Foundation
 * @license    https://opensource.org/licenses/MIT	MIT License
 * @link       https://codeigniter.com
 * @since      Version 4.0.0
 * @filesource
 *
 * @codeCoverageIgnore
 */

return [
	// Core Messages
   'noRuleSets'            => 'Validointimäärityksessä ei ole määritetty sääntöjä.',
   'ruleNotFound'          => '{0} ei ole kelvollinen sääntö.',
   'groupNotFound'         => '{0} ei ole validointisääntöryhmä.',
   'groupNotArray'         => '{0} sääntöryhmän on oltava taulukko.',
   'invalidTemplate'       => '{0} ei ole kelvollinen vahvistusmalli.',

	// Rule Messages
   'alpha'                 => ' {field} -kenttä saa sisältää vain kirjaimia.',
   'alpha_dash'            => ' {field} -kentässä voi olla vain aakkosnumeerisia, alaviiva- ja viivamerkkejä.',
   'alpha_numeric'         => ' {field} -kenttä voi sisältää vain aakkosnumeerisia merkkejä.',
   'alpha_numeric_punct'   => ' {field} -kenttä voi sisältää vain aakkosnumeerisia merkkejä, välilyöntejä ja ~! # $% & * - _ + = | :. merkkejä.',
   'alpha_numeric_space'   => ' {field} -kenttä voi sisältää vain aakkosnumeerisia ja välilyöntejä.',
   'alpha_space'           => ' {field} -kenttä voi sisältää vain aakkosellisia merkkejä ja välilyöntejä.',
   'decimal'               => ' {field} -kentän tulee sisältää desimaaliluku.',
   'differs'               => ' {field} -kentän on poikettava {param} -kentästä.',
   'equals'                => ' {field} -kentän on oltava tarkalleen samanlainen kuin: {param}.',
   'exact_length'          => ' {field} -kentän on oltava tarkalleen {param} merkkiä pitkä.',
   'greater_than'          => ' {field} -kentän tulee sisältää luku, joka on suurempi kuin {param}.',
   'greater_than_equal_to' => ' {field} -kentän tulee sisältää luku, joka on suurempi tai yhtä suuri kuin {param}.',
   'hex'                   => ' {field} -kenttä voi sisältää vain heksadesimaalimerkkejä.',
   'in_list'               => ' {field} -kentän on oltava jokin seuraavista: {param}.',
   'integer'               => ' {field} -kentän tulee sisältää kokonaisluku.',
   'is_natural'            => ' {field} -kentässä saa olla vain numeroita.',
   'is_natural_no_zero'    => ' {field} -kentässä saa olla vain numeroita ja sen on oltava suurempi kuin nolla.',
   'is_not_unique'         => ' {field} -kentän on sisällettävä aiemmin olemassa oleva arvo tietokannassa.',
   'is_unique'             => ' {field} -kentän on sisällettävä yksilöivä arvo.',
   'less_than'             => ' {field} -kentän tulee sisältää vähemmän kuin {param}.',
   'less_than_equal_to'    => ' {field} -kentän tulee sisältää luku, joka on pienempi tai yhtä suuri kuin {param}.',
   'matches'               => ' {field} -kenttä ei vastaa {param} -kenttää.',
   'max_length'            => ' {field} -kentässä saa olla enintään {param} merkkiä.',
   'min_length'            => ' {field} kentän on oltava vähintään {param} merkkiä pitkä.',
   'not_equals'            => ' {field} kenttä ei voi olla: {param}.',
   'numeric'               => ' {field} -kentän tulee sisältää vain numeroita.',
   'regex_match'           => ' {field} -kenttä ei ole oikeassa muodossa.',
   'required'              => ' {field} -kenttä vaaditaan.',
   'required_with'         => ' {field} -kenttä vaaditaan, kun {param} on läsnä.',
   'required_without'      => ' {field} -kenttä vaaditaan, kun {param} ei ole läsnä.',
   'timezone'              => ' {field} -kentän on oltava kelvollinen aikavyöhyke.',
   'valid_base64'          => ' {field} -kentän on oltava kelvollinen base64-merkkijono.',
   'valid_email'           => ' {field} -kentän on sisällettävä kelvollinen sähköpostiosoite.',
   'valid_emails'          => ' {field} -kentän on sisällettävä kaikki voimassa olevat sähköpostiosoitteet.',
   'valid_ip'              => ' {field} -kentän on sisällettävä kelvollinen IP-osoite.',
   'valid_url'             => ' {field} -kentän on sisällettävä kelvollinen URL-osoite.',
   'valid_date'            => ' {field} -kentän on oltava kelvollinen päivämäärä.',

	// Credit Cards
   'valid_cc_num'          => ' {field} -ei näytä olevan kelvollinen luottokorttinumero.',

	// Files
   'uploaded'              => ' {field} -ei ole kelvollinen ladattu tiedosto.',
   'max_size'              => ' {field} -Tiedoston koko on liian suuri.',
   'is_image'              => ' {field} -ei ole kelvollinen, ladattu kuvatiedosto.',
   'mime_in'               => ' {field} -ei ole kelvollista mime-tyyppiä.',
   'ext_in'                => ' {field} -ei ole kelvollista tiedostotunnistetta.',
   'max_dims'              => ' {field} -ei ole joko kuva tai se on liian leveä tai pitkä.',
];
