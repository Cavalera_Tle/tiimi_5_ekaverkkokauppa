<div class="row content_background">
    <div class="container text-center px-3 pt-3 pb-3">

        <h1> Kiitos tilauksesta!</h1>

        <p>Tilauksenne on nyt vastaanotettu.</p>
        <p>Toimitamme tilauksen 1-3 arkipäivän sisällä.</p>

    </div>
</div>