<div class="row content_background">
    <div class="container text-center px-3 pt-3 pb-3" >
        <form action="/tilaus/tilaaminen" method="post">
        <div class="text-center validointi">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
            <div class="col-12 text-center">
                <h1><?= $title ?></h1>
            </div>
            <div class="form-row">
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 300px">
                    <label>Etunimi</label>
                    <input class="form-control" name="etunimi" placeholder="Etunimi" maxlength="30" required/>
                </div>
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 300px">
                    <label>Sukunimi</label>
                    <input class="form-control" name="sukunimi" placeholder="Sukunimi" maxlength="100" required/>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 300px">
                    <label>Lähiosoite</label>
                    <input class="form-control" name="lahiosoite" placeholder="Lähiosoite" maxlength="100" required/>
                </div>
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 300px">
                    <label>Postinumero</label>
                    <input class="form-control" name="postinumero" placeholder="Postinumero" maxlength="5" required/>
                </div>
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 300px">
                    <label>Postitoimipaikka</label>
                    <input class="form-control" name="postitoimipaikka" placeholder="Postitoimipaikka" maxlength="100" required/>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-6 col-md-6 col-sm-12" style="max-width: 300px">
                    <label>Sähköpostiosoite</label>
                    <input class="form-control" name="email" placeholder="Sähköpostiosoite" maxlength="255" required/>
                </div>
                <div class="col-6 col-md-6 col-sm-12" style="max-width: 300px">
                    <label>Puhelinnumero</label>
                    <input class="form-control" name="puhelin" placeholder="Puhelinnumero" maxlength="20" required/>
                </div>
            </div>

            <hr class="mb-4">

            <h1 class="mb-3">Maksunvalinta</h1>

            <div class="d-block my-3"> 
              <label class="radio-inline">
                <input type="radio" name="maksutapa" checked> Pankkikortti 
              </label>
              <label class="radio-inline">
                <input type="radio" name="maksutapa"> Luottokortti 
              </label>
              <label class="radio-inline">
                <input type="radio" name="maksutapa"> PayPal 
              </label>
            </div>

            <div class="row">
            <div class="form-group col-6 col-md-6 col-sm-12" style="max-width: 400px">
                <label for="haltija">Kortin haltijan nimi</label>
                <input type="text" class="form-control" id="haltija" placeholder="Nimi kortissa" required>
                <div class="invalid-feedback">
                  Kortin haltijan nimi on pakollinen tieto
                </div>
              </div>
              <div class="form-group col-6 col-md-6 col-sm-12" style="max-width: 400px">
                <label for="numero">Kortin numero</label>
                <input type="text" class="form-control" id="numero" placeholder="Kortin numero" required>
                <div class="invalid-feedback">
                  Kortin numero on pakollinen tieto
                </div>
              </div>
            </div>
            <div class="row">
            <div class="form-group col-6 col-md-6 col-sm-12" style="max-width: 400px">
                <label for="voimassaolo">Voimassaolo päivämäärä</label>
                <input type="text" class="form-control" id="voimassaolo" placeholder="voimassaolo päivämäärä" required>
                <div class="invalid-feedback">
                  Voimassaolo päivämäärä on pakollinen
                </div>
              </div>
              <div class="form-group col-6 col-md-6 col-sm-12" style="max-width: 400px">
                <label for="cw">CW</label>
                <input type="text" class="form-control" id="cw" placeholder="CW" required>
                <div class="invalid-feedback">
                  CW pakollinen
                </div>
              </div>
            </div>
            <hr class="mb-4">

            <div class="form-row">
                <div class="form-group col-4 ml-5">
                    <input class="form-check-input" type="checkbox" value="" id="tietosuoja">
                    <label class="form-check-label" for="tietosuoja">
                            Olen lukenut ja hyväksynyt <a href=""><span text="danger">tietosuojaselosteen</span></a>
                    </label>
                </div>
                    <div class="form-group col-3">
                        <button class="btn lahetanappi">Maksa ja tilaa</button>
                    </div>
            </div>
        </form>
      </div>    
    </div>