<div class="row content_background">
    <div class="container text-center px-3 pt-3 pb-3">
        <form action="/tilaus/tallennus" method="post">
        <div class="text-center validointi">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
            <div class="col-12 text-center">
                <h1><?= $title ?></h1>
            </div>
            <h2>Tarkista henkilötiedot</h2>
            <div class="col-12 text-center table-responsive">
                <table class="table lomake_tausta">
                    <?php foreach($asiakas as $tieto): ?>                      
                        <input type="hidden" name="id" value="<?= $tieto['id']?>"/>
                        <input type="hidden" name="kayttaja" value="<?= $tieto['kayttaja']?>"/>          
                        <tr>
                            <th>Etunimi</th>
                            <th>Sukunimi</th>
                            <th>Lähiosoite</th>
                            <th>Postinumero</th>
                            <th>Postitoimipaikka</th>
                            <th>Sähköposti</th>
                            <th>Puhelin</th>
                        </tr>
                        <tr>
                            <td>                    
                                <?= $tieto['etunimi'] ?>
                            </td>
                            <td>    
                                <?= $tieto['sukunimi'] ?>
                            </td>
                            <td>    
                                <?= $tieto['lahiosoite'] ?>
                            </td>
                            <td>    
                                <?= $tieto['postinumero'] ?>
                            </td>
                            <td>    
                                <?= $tieto['postitoimipaikka'] ?>
                            </td>
                            <td>    
                                <?= $tieto['email'] ?>
                            </td>
                            <td>    
                                <?= $tieto['puhelin'] ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </table>
            </div>        
            <h2>Ostoskorin sisältö</h2>
            <div class="text-center table-responsive">           
                 <table class="table lomake_tausta" >
                    <tr>
                        <th>Nimi</th>
                        <th>Kappalemäärä</th>
                        <th>Hinta</th>
                    </tr>
                    <?php $summa = 0; ?>
                    <?php foreach ($ostokset as $ostos) : ?>
                    <tr>
                        <td>
                            <?= $ostos['nimi'] ?>
                        </td>
                        <td>
                            <?= $ostos['maara'] ?>
                        </td>
                        <td>
                            <?= $ostos['hinta'] * $ostos['maara'] ?>
                        </td>
                    </tr>    
                        <?php $summa += $ostos['hinta'] * $ostos['maara']; ?>
                    <?php endforeach ?>
                    <tr>
                        <td></td>
                        <td>
                            <b>Yhteensä</b>
                        </td>
                        <td>
                            <b><?= $summa ?>€</b>
                        </td>
                    </tr>
                </table>    
            </div>            
            <button class="btn"><a href="/tilaus">Edellinen</a></button>
               
            <button class="btn"><a href="/ostoskori">Peruuta</a></button>

            <button class="btn lahetanappi">Vahvista tilaus</button>

        </form>
    </div>
</div>