<div class="row content_background">
    <div class="container text-center px-3 pt-3 pb-3" >
    <?php foreach ($asiakas as $asiakas): ?>
        <form action="/tilaus/tilaaminen" method="post">
        <div class="text-center validointi">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
            
          <div class="col-lg-12 text-center">
                <h1><?= $title ?></h1>
            </div>
            <input type="hidden" name="id" value="<?= $asiakas->id?>" />
            <input type="hidden" name="kayttaja" value="<?= $asiakas->kayttaja?>" />
            
           
          <div class="form-row">
            <div class="form-group col-lg-3 col-md-6 col-sm-12" style="max-width: 250px">
                <label>Etunimi</label>
                <input class="form-control" name="etunimi" placeholder="Etunimi" maxlength="30" value="<?= $asiakas->etunimi?>"/>
            </div>
            <div class="form-group col-lg-3 col-md-6 col-sm-12" style="max-width: 250px">
              <label>Sukunimi</label>
              <input class="form-control" name="sukunimi" placeholder="Sukunimi" maxlength="100" value="<?= $asiakas->sukunimi?>"/>
            </div>
            <div class="form-group col-lg-3 col-md-6 col-sm-12" style="max-width: 250px">
              <label>Sähköpostiosoite</label>
              <input class="form-control" name="email" placeholder="Sähköpostiosoite" maxlength="255" value="<?= $asiakas->email?>"/>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12" style="max-width: 250px">
              <label>Puhelinnumero</label>
              <input class="form-control" name="puhelin" placeholder="Puhelinnumero" maxlength="20" value="<?= $asiakas->puhelin?>"/>
            </div>
          </div>

            <div class="form-row">
                <div class="col-lg-2"></div>
                <div class="form-group col-lg-3 col-md-6 col-sm-12" style="max-width: 250px">
                    <label>Lähiosoite</label>
                    <input class="form-control" name="lahiosoite" placeholder="Lähiosoite" maxlength="100" value="<?= $asiakas->lahiosoite?>"/>
                </div>
                <div class="form-group col-lg-3 col-md-6 col-sm-12" style="max-width: 250px">
                    <label>Postinumero</label>
                    <input class="form-control" name="postinumero" placeholder="Postinumero" maxlength="5" value="<?= $asiakas->postinumero?>"/>
                </div>
                <div class="form-group col-lg-3 col-md-6 col-sm-12" style="max-width: 250px">
                    <label>Postitoimipaikka</label>
                    <input class="form-control" name="postitoimipaikka" placeholder="Postitoimipaikka" maxlength="100" value="<?= $asiakas->postitoimipaikka?>"/>
                </div>
            </div>
            <div class="col-lg-1"></div>
            
            <hr class="mb-4">

            <h1 class="mb-3">Maksunvalinta</h1>

            <div class="d-block my-3 text-center"> 
              <label class="radio-inline">
                <input type="radio" name="maksutapa" checked> Pankkikortti 
              </label>
              <label class="radio-inline">
                <input type="radio" name="maksutapa"> Luottokortti 
              </label>
              <label class="radio-inline">
                <input type="radio" name="maksutapa"> PayPal 
              </label>
            </div>

            <div class="row">
            <div class="col-lg-3"></div>
            <div class="form-group col-lg-3 col-md-6 col-sm-12" style="max-width: 300px">
                <label for="haltija">Kortin haltijan nimi</label>
                <input type="text" class="form-control" id="haltija" placeholder="Nimi kortissa" required>
                <div class="invalid-feedback">
                  Kortin haltijan nimi on pakollinen tieto
                </div>
              </div>
              <div class="form-group col-lg-3 col-md-6 col-sm-12" style="max-width: 300px">
                <label for="numero">Kortin numero</label>
                <input type="text" class="form-control" id="numero" placeholder="Kortin numero" required>
                <div class="invalid-feedback">
                  Kortin numero on pakollinen tieto
                </div>
              </div>
            </div>
            <div class="col-lg-3"></div>

            <div class="row">
            <div class="col-lg-3"></div>
            <div class="form-group col-lg-3 col-md-6 col-sm-12 ml-4" style="max-width: 240px">
                <label for="voimassaolo">Voimassaolo päivämäärä</label>
                <input type="text" class="form-control" id="voimassaolo" placeholder="voimassaolo päivämäärä" required>
                <div class="invalid-feedback">
                  Voimassaolo päivämäärä on pakollinen
                </div>
              </div>
              <div class="form-group col-lg-3 col-md-6 col-sm-12 ml-5 pl-5" style="max-width: 170px">
                <label for="cw">CW</label>
                <input type="text" class="form-control" id="cw" placeholder="CW" required>
                <div class="invalid-feedback">
                  CW pakollinen
                </div>
              </div>
            </div>
            <div class="col-lg-3"></div>
            <hr class="mb-4">

            <div class="form-row">
                <div class="form-group col-12 ml-2">
                    <input class="form-check-input" type="checkbox" value="" id="tietosuoja">
                    <label class="form-check-label" for="tietosuoja">
                            Olen lukenut ja hyväksynyt <a href=""><span text="danger">tietosuojaselosteen</span></a>
                    </label>
                </div>
                    <div class="form-group col-lg-12 col-md-3 col-sm-3 mt-3">
                      <button class="btn lahetanappi">Maksa ja tilaa</button>
                      <button class="btn lahetanappi ml-3"><a href="/ostoskori">Palaa</a></button>
                    </div>
                    
            </div>
           
        </form>
        <?php endforeach; ?>
       
      </div>    
    </div>