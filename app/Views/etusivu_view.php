<div class="row content_background">
    <div class="col-12 px-3 pt-3 pb-3">
        <h1 class="text-center">KilpiKuoret</h1>
        <div class="row">
            <div class="col-lg-3">
                <div class="text-left sivuteksti text-left">
                    <?php foreach ($tuoteryhmat as $tuoteryhma) : ?>
                        <p><?= anchor('/tuote/selaaTuotteita/' . $tuoteryhma->id, $tuoteryhma->nimi, 'class=" sivupalkkiteksti text-muted"'); ?></p>
                    <?php endforeach ?></div>
            </div>

            <div class="col-lg-9 text-left">
                <ul>
                    <li class="iskulause1">
                        Laaja valikoima suojakuoria, popsocketteja
                        <p class="iskulause22">ja muita puhelintarvikkeita </p>
                    </li>

                    <li class="iskulause2">
                        Nopea ja ilmainen kotiinkuljetus
                    </li>

                    <li class="iskulause2">
                        Me myös huollamme puhelimia
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--MODALit alkavat tästä -->
<div class="modal fade text-center" data-keyboard="false" data-backdrop="static" id="Kirjautunut" tabindex="-1" role="dialog" aria-labelledby="modaltitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <h5 class="modal-title" id="modaltitle">Kirjautuminen onnistui!</h5>
                <p>Tervetuloa KilpiKuoret verkkokauppaan</p>
                <p>Toivottavasti löydät kaiken tarvitsemasi meiltä</p>

                <div class="buttoni">
                    <button type="button" class="btn btn-secondary" onclick="location.href='/etusivu';" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade text-center" data-keyboard="false" data-backdrop="static" id="UlosKirjaudu" tabindex="-1" role="dialog" aria-labelledby="modaltitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="modal-title" id="modaltitle">Olet kirjautunut ulos!</h5>
                <p>Tervetuloa uudelleen KilpiKuoret verkkokauppaan</p>
                <div class="buttoni">
                    <button type="button" class="btn btn-secondary" onclick="location.href='/etusivu';" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>
</div>