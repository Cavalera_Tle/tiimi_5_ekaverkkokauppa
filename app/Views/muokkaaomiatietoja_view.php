<div class="row content_background">
    <div class="container text-center px-3 pt-3 pb-3">
        <form action="/MuokkaaOmiaTietoja/muokkaa" method="POST">
        <div class="text-center validointi">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
            <div class="col-12 text-center">
                <h1><?= $title ?></h1>
            </div>

            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">

                <input type="hidden" name="id" value="<?= $id?>" />
                <div class="form-group col-lg-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot"><?= lang('muokkaaomiatietoja.kayttaja')?></label>
                    <input class="form-control tiedot2" name="kayttaja" placeholder="Käyttäjänimi" maxlength="30" value="<?= $kayttaja?>" required/>
                </div>
                <div class="form-group col-lg-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot"><?= lang('muokkaaomiatietoja.enimi')?></label>
                    <input class="form-control tiedot2" name="enimi" placeholder="Etunimi" maxlength="30" value="<?= $enimi ?>" required/>
                </div>
                <div class="form-group col-lg-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot"><?= lang('muokkaaomiatietoja.snimi')?></label>
                    <input class="form-control tiedot2" name="snimi" placeholder="Sukunimi" maxlength="100" value="<?= $snimi ?>" required/>
                </div>
            </div>
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
           </div>

            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
                <div class="form-group col-lg-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot"><?= lang('muokkaaomiatietoja.losoite')?></label>
                    <input class="form-control tiedot2" name="lahiosoite" placeholder="Lähiosoite" maxlength="100" value="<?= $lahiosoite ?>" required/>
                </div>
                <div class="form-group col-lg-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot"><?= lang('muokkaaomiatietoja.pnro')?></label>
                    <input class="form-control tiedot2" name="postinumero" placeholder="Postinumero" maxlength="5" value="<?= $postinumero ?>" required/>
                </div>
                <div class="form-group col-lg-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot"><?= lang('muokkaaomiatietoja.ptmp')?></label>
                    <input class="form-control tiedot2" name="postitoimipaikka" placeholder="Postitoimipaikka" maxlength="100" value="<?= $postitoimipaikka ?>" required/>
                </div>
            </div>
                 <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>

            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
                <div class="form-group col-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot"><?= lang('muokkaaomiatietoja.sposti')?></label>
                    <input class="form-control tiedot2" type="text" name="email" placeholder="Sähköpostiosoite" maxlength="255" value="<?= $email ?>"required/>
                </div>
                <div class="col-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot"><?= lang('muokkaaomiatietoja.puhnro')?></label>
                    <input class="form-control tiedot2" name="puhelin" placeholder="Puhelinnumero" maxlength="20" value="<?= $puhelin ?>" required/>
                </div>
               </div>
                <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>

            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
                <div class="form-group col-12 col-md-12 col-sm-12 mt-2" style="max-width: 300px">
                    <label class="tiedot"><?= lang('muokkaaomiatietoja.ss')?></label>
                    <input class="form-control tiedot2" name="salasana" type="password" placeholder="Salasana" maxlength="30" required/>
                </div>

                <div class="form-group col-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot"><?= lang('muokkaaomiatietoja.ssu')?></label>
                    <input class="form-control tiedot2" name="confirmpassword" type="password" placeholder="Vahvista salasana" maxlength="30" required/>
                </div>
                 </div>
                <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>

            <div class="form-row">
           <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
            <div class="form-group col-8 col-md-8 ml-3 mt-2 uutiskirje" style="max-width: 300px"> 
             <p class="mb-1"><?= lang('muokkaaomiatietoja.uk')?></p>
            </div>
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>
            </div>
          
            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
            <div class="form-check form-check-inline mr-5">

                <input class="form-check-input" type="radio" name="uutiskirje" id="kirje_kylla" value="1" checked>
                    <label class="form-check-label" for="kirje_kylla">
                    <?= lang('muokkaaomiatietoja.k')?>
                    </label>
            </div> 
            <div class="form-check form-check-inline pr-5">
                <input class="form-check-input" type="radio" name="uutiskirje" id="kirje_ei" value="0">
                    <label class="form-check-label" for="kirje_ei">
                    <?= lang('muokkaaomiatietoja.e')?>
                    </label>
            </div>
            </div>
            </div>

            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
            <div class="form-group col-8 col-md-8 ml-3 mt-4 uutiskirje" style="max-width: 300px">
            <p class="mb-1"><?= lang('muokkaaomiatietoja.lupa')?></p>
            </div>
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>
            </div>

            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
            <div class="form-check form-check-inline mr-5">
                <input class="form-check-input" type="radio" name="tietojenluovutus" id="luovutus_kylla" value="1" checked>
                    <label class="form-check-label" for="luovutus_kylla">
                    <?= lang('muokkaaomiatietoja.k')?>
                    </label>
            </div> 
            <div class="form-check form-check-inline pr-5">
                <input class="form-check-input" type="radio" name="tietojenluovutus" id="luovutus_ei" value="0">
                    <label class="form-check-label" for="luovutus_ei">
                    <?= lang('muokkaaomiatietoja.e')?>
                    </label>
            </div>
            </div>
            </div>
            
            <div class="form-row">
                <div class="col-12">
                <button class="btn lahetanappi mt-3 mr-5"><?= lang('muokkaaomiatietoja.tallenna')?></button>
                </div>
            </div>

            <div class="form-row ">
               <div class="col-12 text-center">
               <button class="btn mr-5"><a href="/etusivu"><?= lang('muokkaaomiatietoja.etusivu')?></a></button>
               </div>
       </div>
           
        </form>
</div>
        
    </div>
