<section class="row tietoasivu content_background p-1 text-center lomake_tausta">
<div class="col-12">
    <h1>Tietoa yrityksestä <img src="<?=base_url()?>/img/logo.png" class="d-inline-block align-center logo" alt=""></h1>  
    <section class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
    <p class="tietoa">
    KilpiKuoret on vuonna 2020 perustettu verkkokauppa, jossa on myynnissä laaja valikoima lisätarvikkeita
    eri merkkien puhelimille. Haluamme tarjota luotettavan ja helpon tavan tehdä ostoksia ja tilata tuotteet 
    vaikka suoraan kotiovelle asti. </p>

     <p class="tietoa2">Kaikki toimintamme pohjautuu yhdessä sovituille arvoille, joita ovat: </p>
     
         <p class="tietoa1">Asiakaslähtöisyys</p>
         <p>Haluamme, että asiakas on lähtökohtana kaikelle, joten verkkokauppa on tehty mahdollisimman käyttöystävälliseksi
         ja tuotteet valittu huolellisesti.<p>

         <p class="tietoa1">Luotettavuus</p>
         Olemme luotettava suomalainen yritys. Pyrimme pitämään tuotteidemme hinta-laatusuhteen korkeana ja saamamme
         palautteen pohjalta kehitämme toimintaamme jatkuvasti.
         <p class="tietoa1">Joustavuus</p>
         Olemme aina tavoitettavissa, jos ei puhelimitse niin sähköpostin kautta ja pyrimme vastaamaan muutaman 
         tunnin kuluessa kysymyksiinne. Joustavuus näkyy myös mahdollisuutena palauttaa tuote 60 päivän jälkeen
         ostopäätöksestä mikäli sen laatu tai muut kriteerit eivät vastanneet asiakkaan odotuksia. 
    
    </div>
    <div class="col-md-2"></div>
    </section>
 </div>
</section>