<div class="row content_background">
    <?php foreach ($title as $otsikko) : ?>
        <h1 class="col-12 text-center"><?= $otsikko->nimi ?></h1>
    <?php endforeach; ?>  
    <div class="col-1-5"></div>
    <?php foreach ($tuotteet as $tuote) : ?>        
        <div class="col-lg-3 col-md-4 col-sm-6">
            <form method="post" action="<?= site_url('ostoskori/lisaa/' . $tuote->id); ?>">
                <div class="p-2 card my-3 text-center kortti">
                    <img src="<?= base_url() ?>/img/<?= $tuote->kuva ?>" class="img-responsive card-img-top" alt="<?= $tuote->kuva ?>">
                    <div class="card-body">
                        <h5 class="card-title"><?= $tuote->nimi ?></h5>
                        <p class="card-text"><?= $tuote->kuvaus ?></p>
                        <p class="card-text"><small class="text-muted">Hinta: <?= $tuote->hinta ?>€</small> </br><small class="text-muted"> Varastossa: <?= $tuote->varastomaara ?>kpl</small></p>
                        <div class="sticy-bottom">
                        <button class="btn btn-secondary btn-group ostanappi" title="Lisää ostoskoriin">Osta</button>
            </form>
                    <form method="post" action="<?= site_url('wishlist/lisaa/' . $tuote->id); ?>">
                        <button class="btn btn-secondary btn-group sydannappi mt-1" title="Lisää toivelistaan"><i class="fa fa-heart" aria-hidden="true"></i></button>
                    </form>
                        </div>
                    </div>
                </div>
        </div>
    <?php endforeach; ?>
</div>