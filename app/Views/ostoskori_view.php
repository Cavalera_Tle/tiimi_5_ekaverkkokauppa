<div class="row px-3 text-center content_background">

    <div class="col-lg-12 col-md-12">
        <h1 class="col-12 mb-3">Selaa ostoskoria</h1>
        <div class="col-lg-12  text-center table-responsive">

            <table class="table lomake_tausta">
                <!-- Taulukon otsikot -->
                <tr>
                    <th>Tuotteen nimi</th>
                    <th>Vähennä</th>
                    <th>Määrä</th>
                    <th>Lisää</th>
                    <th>Hinta</th>
                </tr>
                <?php $summa = 0; ?>
                <?php foreach ($ostokset as $ostos) : ?>
                    <tr>
                        <td>
                            <?= $ostos['nimi'] ?>
                        </td>
                        <td>
                            <?= anchor('ostoskori/vahenna/' . $ostos['id'], "-"); ?>
                        </td>
                        <td>
                            <?php
                            $lukumaara = 0;
                            foreach ($_SESSION['kori'] as $key => $value) :
                                if ($value == $ostos['id'])
                                    $lukumaara++;
                            endforeach;
                            print $lukumaara;
                            ?>
                        </td>
                        <td>
                            <?= anchor('ostoskori/lisaa/' . $ostos['id'], "+"); ?>
                        </td>

                        <td>
                            <?= $ostos['hinta'] * $lukumaara; ?>
                        </td>
                    </tr>
                    <?php $summa += $ostos['hinta'] * $lukumaara; ?>
                <?php endforeach; ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <b>Yhteensä</b>
                    </td>
                    <td>
                        <b><?= $summa ?>€</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= anchor('ostoskori/tyhjenna/', "Tyhjennä ostoskori"); ?>
                    </td>
                    <td colspan="3"></td>
                    <td>
                        <form action="tilaus" method="post">
                            <div>
                                <button class="btn lahetanappi">Tilaa</button>
                            </div>
                        </form>
                    </td>
            </table>

        </div>

    </div>
</div>