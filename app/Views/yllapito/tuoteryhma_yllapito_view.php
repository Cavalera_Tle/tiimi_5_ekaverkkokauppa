<div class="row yp_content_background">
    <div class="col-12 text-center">
    <h1 class="mb-5">Tuoteryhmien muokkaaminen</h1>
    <div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-4 col-md-12 table-responsive">
        <table class="table">
            <?php foreach ($tuoteryhmat as $tuoteryhma) : ?>
                <tr>
                    <td class="tuoteryhmamuokkaus"><?= $tuoteryhma->id ?></td>
                    <td class="tuoteryhmamuokkaus"><?= $tuoteryhma->nimi ?></td>
                    <td class="tuoteryhmamuokkaus"><?= anchor('tuoteryhmayllapito/update/' . $tuoteryhma->id, "Muokkaa"); ?></td>
                    <td class="tuoteryhmamuokkaus"></td>
                    <td class="tuoteryhmamuokkaus"><?= anchor('tuoteryhmayllapito/delete/' . $tuoteryhma->id, "Poista"); ?></td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>

    <div class="col-lg-4 col-md-12 mt-2">
        <h5>Lisää uusi tuoteryhmä</h5>
        <form action="tuoteryhmaYllapito/lisaa" method="post" class="ml-3">
            <input class="form-control" style="max-width: 280px" type="text" name="tuoteryhma" placeholder="" />
            <button class="btn btn-secondary ostanappi mt-3 mb-3" type=submit value="lisaa"><?= lang('tuoteryhmayllapito.lisaa') ?></button>
        </form>
    </div>
    <div class="col-lg-2"></div>
</div>
</div>
</div>
</div>