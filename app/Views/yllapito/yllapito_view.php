<div class="row yp_content_background">
    <div class="col-12 px-3 pt-3 pb-3">
        <h1 class="text-center">KilpiKuoret - Ylläpito</h1>
        <div class="row">
            <div class="col-lg-5 text-left">
                <ul>
                    <li class="iskulause1">
                        Tervetuloa ylläpitäjä!
                    </li>
                </ul>
            </div>    
            <div class="col-lg-7 text-left">
            <ul>
                <li class="iskulause1">
                    Ylläpitäjänä voit
                </li>
                    <ul>
                        <li class="iskulause2">
                            Lisätä, muokata ja poistaa tuoteryhmiä.
                        </li>
                        <li class="iskulause2">
                            Lisätä, muokata ja poistaa tuotteita.
                        </li>
                        <li class="iskulause2">
                            Hallinnoida tilauksia.
                        </li>
                    </ul>
            </ul>    
            </div>
        </div>
    </div>
</div>
<!--MODALit alkavat tästä -->
<div class="modal fade text-center" data-keyboard="false" data-backdrop="static" id="UlosKirjaudu" tabindex="-1" role="dialog" aria-labelledby="modaltitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="modal-title" id="modaltitle">Olet kirjautunut ulos!</h5>
                <p>Tervetuloa uudelleen KilpiKuoret verkkokauppaan</p>
                <div class="buttoni">
                <button type="button" class="btn btn-secondary" onclick="location.href='/etusivu';" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>
</div>