<div class="row yp_content_background">
    <div class="col-12 text-center">
        <h1>Muokkaa tilauksen tila</h1>
        <p>Syötä tilattu tai toimitettu</p>
     <div class="col-12 text-center">
        <?php foreach ($tilaukset as $tilaus): ?>
            <form method="post" action="/tilausyllapito/muokkaa">
                <div class="form-group"> 
                    <input type="hidden" name="id" value="<?= $tilaus->id ?>"/>
                    <input type="text" name="tila" value="<?= $tilaus->tila ?>"/>
                </div>
                <button class="btn btn-primary" type="submit" value="Tallenna">Lähetä</button>
                <a href="/tilausyllapito/" id="peruuta" name="peruuta" class="btn btn-secondary">Peruuta</a>
            </form>  
        <?php endforeach;?>
    </div>
</div>
