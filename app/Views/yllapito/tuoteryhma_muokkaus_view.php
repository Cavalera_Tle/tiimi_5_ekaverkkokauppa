<div class="row yp_content_background">
    <div class="col-12 text-center">
        <h1>Muokkaa tuoteryhmän nimeä</h1>
     <div class="col-12 text-center">
        <?php foreach ($tuoteryhmat as $tuoteryhma): ?>
            <form method="post" action="/tuoteryhmayllapito/muokkaa">
                    <input type="hidden" name="id" value="<?= $tuoteryhma->id ?>"/>
                <div class="form-group"> 
                    <input type="text" name="tuoteryhma" value="<?= $tuoteryhma->nimi ?>"/>
                </div>
                <button class="btn btn-primary" type="submit" value="Tallenna"><?= lang('tuoteryhmayllapito.tallenna') ?></button>
                <a href="/tuoteryhmayllapito/" id="peruuta" name="peruuta" class="btn btn-secondary"><?= lang('tuoteryhmayllapito.peruuta') ?></a>
            </form>  
        <?php endforeach;?>
    </div>
</div>
