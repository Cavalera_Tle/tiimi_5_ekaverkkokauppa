<div class="row yp_content_background">
    <div>
        <h1 class="col-12 text-center">Tuotteiden muokkaus</h1>
        <br>
        
        <table>
             <tr>
                <th>Id</th>
                <th>Nimi</th>
                <th>Hinta</th>
                <th>Kuvaus</th>
                <th>Varastomäärä</th>
                <th>Kuva</th>
                <th>Tuoteryhmän id</th>
            </tr>
            <?php foreach ($tuotteet as $tuote) : ?>
            <tr>
                <td><?= $tuote->id ?></td>
                <td><?= $tuote->nimi ?></td>
                <td><?= $tuote->hinta ?></td>
                <td><?= $tuote->kuvaus ?></td>
                <td><?= $tuote->varastomaara ?></td>
                <td><?= $tuote->kuva ?></td>
                <td><?= $tuote->tuoteryhma_id ?></td>

                <td><?= anchor('tuoteyllapito/update/' . $tuote->id, "Muokkaa"); ?></td>
                <td></td>
                <td><?= anchor('tuoteyllapito/delete/' . $tuote->id, "Poista"); ?></td>
            </tr>
            <?php endforeach ?>
        </table>
    </div>
        
    <div class="container text-center">
        <h1 class="col-12 text-center">Lisää uusi tuote</h1>
        <div class="col-12 text-center validointi">
            <?= \Config\Services::validation()->listErrors();?>
        </div>
        <form method="POST" enctype="multipart/form-data" action="/tuoteyllapito/lisaa">
            <div class="form-row text-center">
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                    <label><?= lang('tuoteyllapito.tuotenimi')?></label>
                    <input class="form-control" type="text" name="tuotenimi" placeholder="Tuotteen nimi" required />
                </div>
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                    <label><?= lang('tuoteyllapito.hinta')?></label>
                    <input class="form-control" type="decimal" name="hinta" placeholder="Tuotteen hinta" required />
                </div>
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                    <label><?= lang('tuoteyllapito.kuvaus')?></label>
                    <input class="form-control" type="text" name="kuvaus" placeholder="Tuotekuvaus" required />
                </div>
             </div>
            <div class="form-row text-center">
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                    <label><?= lang('tuoteyllapito.varastomaara')?></label>
                    <input class="form-control" type="number" name="varastomaara" placeholder="Varastomäärä" required />
                </div>
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                    <label><?= lang('tuoteyllapito.kuva')?></label>
                    <input class="form-control" type="file" name="kuva" placeholder="Kuva"/>
                </div>
                <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                    <label><?= lang('tuoteyllapito.tuoteryhma_id')?></label>
                    <input class="form-control" type="number" name="tuoteryhma_id" placeholder="Tuoteryhmän id" required />
                </div>
            </div>            
            <button class="btn btn-secondary mb-3" type="submit" value="lisaa" style="width: 200px"><?= lang('tuoteyllapito.lisaa')?></button>
        </form>
    </div>
</div>