<div class="row yp_content_background">
    <div class="container">
        <h1 class="col-12 text-center">Muokkaa tuotteen tietoja</h1> <br>
        <div class="text-center validointi">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
        <form method="POST" enctype="multipart/form-data" action="/tuoteyllapito/muokkaa">
            <?php foreach ($tuotteet as $tuote) : ?>
                <input type="hidden" name="id" value="<?= $tuote->id ?>" />
                <div class="form-row text-center">
                    <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                        <label><?= lang('tuoteyllapito.tuotenimi') ?></label>
                        <input class="form-control" type="text" name="tuotenimi" value="<?= $tuote->nimi ?>" placeholder="Tuotteen nimi" required />
                    </div>
                    <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                        <label><?= lang('tuoteyllapito.hinta') ?></label>
                        <input class="form-control" type="decimal" value="<?= $tuote->hinta ?>" name="hinta" placeholder="Tuotteen hinta" required />
                    </div>
                    <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                        <label><?= lang('tuoteyllapito.kuvaus') ?></label>
                        <input class="form-control" type="text" name="kuvaus" value="<?= $tuote->kuvaus ?>" placeholder="Tuotekuvaus" required />
                    </div>
                </div>
                <div class="form-row text-center">
                    <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                        <label><?= lang('tuoteyllapito.varastomaara') ?></label>
                        <input class="form-control" type="number" name="varastomaara" value="<?= $tuote->varastomaara ?>" placeholder="Varastomäärä" required />
                    </div>
                    <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                        <label><?= lang('tuoteyllapito.tuoteryhma_id') ?></label>
                        <input class="form-control" type="number" name="tuoteryhma_id" value="<?= $tuote->tuoteryhma_id ?>" placeholder="Tuoteryhmän id" required />
                    </div>
                    <div class="form-group col-4 col-md-6 col-sm-12" style="max-width: 350px">
                        <label><?= lang('tuoteyllapito.kuva') ?></label>
                        <input class="form-control" type="text" name="kuva" value="<?= $tuote->kuva ?>" placeholder="Kuva" />
                    </div>
                </div>
                <div class="form-row text-center">
                    <div class="form-group col-6 col-md-6 col-sm-12">
                        <button class="btn btn-secondary mb-3 mr-3" style="width: 200px" type="submit" value="Tallenna"><?= lang('tuoteyllapito.tallenna') ?></button>
                    </div>
                    <div class="form-group col-6 col-md-6 col-sm-12">
                        <a href="/tuoteyllapito/" id="peruuta" name="peruuta" class="btn btn-secondary mb-3" style="width: 200px"><?= lang('tuoteyllapito.peruuta') ?></a>
                    </div>
                </div>
            <?php endforeach; ?>
        </form>
    </div>
</div>