<div class="row yp_content_background lomake_tausta">
    <div class="col-12 table-responsive text-center">
        <h1>Tilausten käsittely</h1>
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Tilausaika</th>
                <th>Tila</th>
                <th></th>
                <th>Asiakas Id</th>
            </tr>
            <?php foreach ($tilaukset as $tilaus) : ?>
            <tr>
                <td><?= $tilaus->id ?></td>
                <td><?= $tilaus->tilattu ?></td>
                <td><?= $tilaus->tila ?></td>
                <td><?= anchor('tilausyllapito/update/' . $tilaus->id, "Muokkaa tilaa"); ?></td>
                <td><?= $tilaus->asiakas_id ?></td>
            </tr>
            <?php endforeach ?>
        </table>
    </div>
    <div class="col-12 table-responsive text-center">
        <h2>Tilausrivit</h2>
        <table class="table lomake_tausta">
            <tr>
                <th>Tilaus-id</th>
                <th>Tuote-id</th>
                <th>Määrä</th>
            </tr>
            <?php foreach ($tilausrivit as $tilausrivi) : ?>
            <tr>
                <td><?= $tilausrivi->tilaus_id ?></td>
                <td><?= $tilausrivi->tuote_id ?></td>
                <td><?= $tilausrivi->maara ?></td>
            </tr>
            <?php endforeach ?>
        </table>
    </div>
</div>