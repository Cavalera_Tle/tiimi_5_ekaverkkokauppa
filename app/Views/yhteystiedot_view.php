<div class="row content_background">
<div class="px-3 text-center col-lg-12 col-md-12 col-sm-12">
    <!--Section: Contact v.1-->
<section class="section pb-5">

<!--Section heading-->
<h1 class="section-heading h1 pt-4">Yhteystiedot</h1>
<!--Section description-->
<p class="section-description pb-4"> Voit ottaa meihin yhteyttä esimerkiksi alla olevan palautelomakkeen kautta
  tai soittamalla alalaidassa olevaan numeroon.<br> Jos sinulla on jotakin kysyttävää, lähetä sähköpostia. Vastaamme
 mielellämme kysymyksiinne. </p>

<div class="row">

  <!--Grid column-->
  <div class="col-lg-5 col-md-12 col-sm-12 mb-4">

    <!--Form with header-->
    <div class="card content_background col-lg-12 col-md-12 col-sm-12">

      <div class="card-body">
        <!--Header-->
        <div class="form-header blue accent-1">
          <h3><i class="fas fa-envelope"></i> Anna meille palautetta:</h3>
        </div>

        <p>Palautteesi on meille tärkeää!</p>
        <br>

        <!--Body-->
        <form action="/etusivu/palaute">
        <label for="form-name"><i class="fas fa-user prefix grey-text"></i> <?= lang('palaute.nimi')?></label>
        <div class="input-group mb-2">
          <input type="text" name="form-name" class="form-control" required style="width: 300px">
        </div>

        <label for="form-email"> <i class="fas fa-envelope prefix grey-text"></i>   <?= lang('palaute.sposti')?></label>
        <div class="input-group mb-2">
          <input type="text" name="form-email" class="form-control" required style="width: 300px">
        </div>

        <label for="form-subject"><i class="fas fa-tag prefix grey-text"></i>   <?= lang('palaute.otsikko')?></label>
        <div class="input-group mb-2">
          <input type="text" name="form-subject" class="form-control" required style="width: 300px">
        </div>

        <label for="form-text"><i class="fas fa-pencil-alt prefix grey-text"></i>   <?= lang('palaute.viesti')?></label>
        <div class="input-group mb-2">
          <textarea name="form-text" class="form-control md-textarea" rows="5" required style="width: 300px"></textarea>
        </div>

        <!-- LÄHETÄ NAPPI -->
        <div class="text-center mt-4">
          <button class="btn btn-info"><?= lang('palaute.laheta')?></button>
        </div>

                <!-- MODAL -->
          <div class="modal fade" id="kiitospalaute" tabindex="-1" role="dialog" aria-labelledby="modaltitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                
                <div class="modal-body">
                <h5 class="modal-title" id="modaltitle">Kiitos palautteestasi!</h5>
                <i class="fa fa-smile-o" aria-hidden="true"></i>
                <div class="buttoni">
                <button type="button" class="btn btn-secondary"  data-dismiss="modal">Sulje</button>
                </div>
                </div>
          
              </div>
            </div>
          </div>
    </form>
    </div>

    </div>
    <!--Form with header-->

  </div>
  <!--Grid column-->

  <!--Grid column-->
  <div class="col-lg-7 col-md-12 col-sm-12">

    <!--Google map-->
    <div id="map-container-google-11" class="z-depth-1-half map-container-6 mt-4" style="height: 400px">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1682.4255720735052!2d25.464104816310314!3d65.05931768401616!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46802d0fee05afb3%3A0xe8963f6b08a1eba1!2sUniversity%20of%20Oulu!5e0!3m2!1sen!2sfi!4v1587709463255!5m2!1sen!2sfi" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

    <br>
    <!--Buttons-->
    <div class="row text-center">
      <div class="col-md-4">
        <a class="btn-floating blue accent-1"><i class="fas fa-map-marker-alt"></i></a>
        <a class="btn-floating blue accent-1">Osoite</a>
        <p><?= lang('palaute.myymala')?></p>
        <p><?= lang('palaute.maa')?></p>
      </div>

      <div class="col-md-4">
        <a class="btn-floating blue accent-1"><i class="fas fa-phone"></i></a>
        <a class="btn-floating blue accent-1">Puhelinnumero</a>
        <p><?= lang('palaute.myymalapnro')?></p>
        <a class="btn-floating blue accent-1">Aukioloajat</a>
        <p><?= lang('palaute.auki')?></p>
      </div>

      <div class="col-md-4">
        <a class="btn-floating blue accent-1"><i class="fas fa-envelope"></i></a>
        <a class="btn-floating blue accent-1">Asiakaspalvelu</a>
        <p><?= lang('palaute.infosposti')?></p>
        <a class="btn-floating blue accent-1"><i class="fas fa-envelope"></i></a>
        <a class="btn-floating blue accent-1">Myynti</a>
        <p><?= lang('palaute.salesposti')?></p>
      </div>
    </div>
  </div>
</div>
</section>
</div>
</div>