<div class="row content_background">
    <div class="container text-center px-3 pt-3 pb-3">
        <form action="/rekisteroidy/rekisterointi" method="POST">
        <div class="text-center validointi">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
            <div class="col-12 text-center">
                <h1>Rekisteröidy asiakkaaksi</h1>
            </div>

            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">

                <div class="form-group col-lg-12 col-md-12 col-sm-12 " style="max-width: 300px">
                    <label class="tiedot">Käyttäjänimi</label>
                    <input class="form-control" name="kayttaja" placeholder="Vähintään 8 merkkiä" maxlength="30">
                </div>
                <div class="form-group   col-lg-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot">Etunimi</label>
                    <input class="form-control" name="enimi" placeholder="Etunimi" maxlength="30">
                </div>
                <div class="form-group   col-lg-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot">Sukunimi</label>
                    <input class="form-control" name="snimi" placeholder="Sukunimi" maxlength="100">
                </div>
            </div>
             <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>
            
            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
                <div class="form-group   col-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot">Lähiosoite</label>
                    <input class="form-control" name="lahiosoite" placeholder="Lähiosoite" maxlength="100">
                </div>
                <div class="form-group   col-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot">Postinumero</label>
                    <input class="form-control" name="postinumero" placeholder="Postinumero" maxlength="5">
                </div>
                <div class="form-group   col-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot">Postitoimipaikka</label>
                    <input class="form-control" name="postitoimipaikka" placeholder="Postitoimipaikka" maxlength="100">
                </div>
                </div>
                <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>

            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
                <div class="form-group   col-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot">Sähköpostiosoite</label>
                    <input class="form-control" name="email" placeholder="Sähköpostiosoite" maxlength="255">
                </div>
                <div class="  col-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot">Puhelinnumero</label>
                    <input class="form-control" name="puhelin" placeholder="Puhelinnumero" maxlength="20">
                </div>
            </div>
                <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>

            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
                <div class="form-group   col-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot">Salasana</label>
                    <input class="form-control" name="salasana" type="password" placeholder="Salasana" maxlength="30">
                </div>

                <div class="form-group   col-12 col-md-12 col-sm-12" style="max-width: 300px">
                    <label class="tiedot">Vahvista salasana</label>
                    <input class="form-control" name="confirmpassword" type="password" placeholder="Vahvista salasana" maxlength="30">
                </div>
                </div>
                <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>


           <div class="form-row">
           <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
            <div class="form-group col-8 col-md-8 ml-3 uutiskirje" style="max-width: 300px"> 
             <p class="mb-1">Haluatko tilata uutiskirjeemme?</p>
            </div>
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>
            </div>
          
            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
            <div class="form-check form-check-inline mr-5">

                <input class="form-check-input" type="radio" name="uutiskirje" id="kirje_kylla" value="1" checked>
                    <label class="form-check-label" for="kirje_kylla">
                        Kyllä
                    </label>
            </div> 
            <div class="form-check form-check-inline pr-5">
                <input class="form-check-input" type="radio" name="uutiskirje" id="kirje_ei" value="0">
                    <label class="form-check-label" for="kirje_ei">
                        Ei
                    </label>
            </div>
            </div>
            </div>


            <div class="form-row">
           <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
            <div class="form-group col-8 col-md-8 ml-3 uutiskirje" style="max-width: 300px"> 
             <p class="mb-1">Saako tietojasi luovuttaa kolmansille osapuolille?</p>
            </div>
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            </div>
            </div>

          
            <div class="form-row">
            <div class="col-lg-4 col-md-1 col-sm-1"></div>
            <div class="col-lg-4 col-md-10 col-sm-10">
            
            <div class="form-check form-check-inline mr-5">
                <input class="form-check-input" type="radio" name="tietojenluovutus" id="luovutus_kylla" value="1" checked>
                    <label class="form-check-label" for="luovutus_kylla">
                        Kyllä
                    </label> 
            </div>
            <div class="form-check form-check-inline pr-5">
                <input class="form-check-input" type="radio" name="tietojenluovutus" id="luovutus_ei" value="0">
                    <label class="form-check-label" for="luovutus_ei">
                        Ei
                    </label>
            </div>
            </div>
            </div>

            


            <div class="form-row">
             <div class="col-12">
            <button class="btn lahetanappi mt-3 mr-5">Lähetä</button>
            </div>
            </div>


        </form>
    </div>
</div>
    <br>
