<div class="row content_background">
    <div class="container col-12 px-3 pt-3 pb-3">
        <form class="form" action="/kirjautuminen/tarkista" method="POST">
        <div class="text-center validointi">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
            <div class="col-12 text-center">
                <h1><?= $title ?></h1>
            </div>
            <!--MODALit alkavat tästä -->
            <div class="modal fade text-center" data-keyboard="false" data-backdrop="static" id="vaaraTunnusTaiSs" tabindex="-1" role="dialog" aria-labelledby="modaltitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-body">
                            <h5 class="modal-title" id="modaltitle">VIRHE!</h5>
                            <p>Antamasi tunnus tai salasana oli virheellinen</p>
                            <div class="buttoni">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Yritä uudelleen</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal fade text-center" data-keyboard="false" data-backdrop="static" id="Rekisteroityminen" tabindex="-1" role="dialog" aria-labelledby="modaltitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-body">
                            <h5 class="modal-title" id="modaltitle">Tili luotu onnistuneesti!</h5>
                            <p>Jatka kirjautumalla sisälle</p>
                            <div class="buttoni">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tämä Selvä</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal fade" id="tietojenmuokkaus" tabindex="-1" role="dialog" aria-labelledby="modaltitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h5 class="modal-title" id="modaltitle">Tiedot tallennettu onnistuneesti!</h5>
                <p>Sinut on kirjattu ulos, jotta tiedot päivittyvät järjestelmäämme</p>
                <p>Ole hyvä ja kirjaudu uudestaan sisälle kauppaan</p>
                <p>jatkaaksesi ostoksia</p>
                <i class="fa fa-smile-o" aria-hidden="true"></i>
                <div class="buttoni">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sulje</button>
                </div>
            </div>
        </div>
    </div>
</div>
            <!--MODALit loppuvat tähän-->

            <div class="form-row text-center">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="form-group col-6" style="max-width: 300px">
                        <label>Käyttäjänimi</label>
                        <input class="form-control" name="kayttaja" placeholder="Syötä käyttäjänimi" maxlength="30" required="">
                    </div>
                    <div class="form-group col-6" style="max-width: 300px">
                        <label>Salasana</label>
                        <input class="form-control" name="salasana" type="password" placeholder="Syötä salasana" maxlength="30" required="">
                    </div>
                </div>
                <div class="col-12 text-center pr-3">
                    <button class="btn btn-outline-secondary btn-light mb-1">Kirjaudu sisään</button><br>
                    <?= anchor('rekisteroidy', 'Rekisteröidy') ?>
                </div>
                <div class="col-md-4"></div>
            </div>

            <br>
        </form>