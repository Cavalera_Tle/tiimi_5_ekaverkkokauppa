<section class="row content_background p-1 text-center lomake_tausta">
    <div class="col-12">
        <h1>Toimitusehdot <img src="<?= base_url() ?>/img/logo.png" class="d-inline-block align-center logo" alt=""></h1>
        <section class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <p class="tietoa1">Yleiset ehdot</p>
                <p>
                    Kilpikuoret (myöh. myyjä) myy tuotteitaan kuluttajille ja suomalaisille yrityksille
                    noudattaen toiminnassaan Suomen lakia. Tilauksen yhteydessä tilaajan
                    tulee antaa suostumuksensa, että myyjä rekisteröi henkilötiedot
                    tuotteiden toimitusta varten asiakasrekisteriinsä.
                    Tilaajan antamat tiedot ovat luottamuksellisia
                    ja myyjä sitoutuu olemaan luovuttamatta tietoja muille.
                </p>

                <p class="tietoa1">Tilausvahvistus</p>
                <p>Vastaanotettuamme tilauksen, tilausvahvistus lähtee automaattisesti sähköpostitse.
                    Mikäli vahvistusta ei tule, pyydämme ottamaan yhteyttä.
                </p>

                <p class="tietoa1"> Tilauksen sitovuus</p>
                <p>Sitova kauppasopimus tulee voimaan, kun myyjä on lähettänyt kirjallisen tilausvahvistuksen.
                    Tilauksen tekemisen jälkeen myyjällä ei ole oikeutta muuttaa sopimusehtoja.</p>

                <p class="tietoa1">Hinnat</p>
                <p>Hintoihin sisältyy 24% arvonlisävero. Hinnat eivät sisällä toimituskuluja.
                    Myyjä pidättää itsellään oikeuden hinnanmuutoksiin.</p>

                <p class="tietoa1">Toimitus</p>
                <p>Postitamme tilauksesi 1-3 arkipäivän kuluessa. Tilaus toimitetaan lähimpään Postiin,
                    josta voit noutaa sen saatuasi saapumisilmoituksen. Yli 50€ tilaukset ilman toimituskuluja. Alle 50€ tilauksissa toimituskulut 5,50€.
                    myyjä ei vastaa Postin viiveistä tai toimituspäivien mahdollisista muutoksista ja niiden seurauksista asiakkaalle.</p>

                    <p class="tietoa1">Vaihto- ja palautusoikeus</p>
                    <p>Kaikilla tuotteilla on kuluttajansuojalain mukainen 14 päivän vaihto- ja palautusoikeus.
                         Palautettu tuote tulee olla käyttämätön, alkuperäispakkauksessa ja samassa kunnossa kuin sitä vastaanotettaessa
                        Palautuksen kustannukset asiakkaalle on 0€.</p>
                        <p>Mikäli lähettämämme tuote on viallinen tai väärä, pyydämme ottamaan välittömästi yhteyttä asiakaspalveluumme kirjallisesti.</p>
            </div>
        </section>
    </div>
</section>