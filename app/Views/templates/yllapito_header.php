<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>KilpiKuoret</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="<?= base_url('/css/style.css'); ?>">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <style>
  body {
    background-image: url('http://getwallpapers.com/wallpaper/full/e/e/d/892611-download-free-white-3d-background-1920x1080-1080p.jpg');
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    background-attachment: fixed;
  }
</style>
</head>
<body class="sisalto">

<!--TÄSTÄ ALKAA NAVBAR -->
<nav class="container sticky-top navbar navbar-expand-lg rounded-top navigointipalkki">
  <a class="navbar-brand mt-1 ml-2" href="/yllapito"> <img src="<?= base_url() ?>/img/logo.png" class="d-inline-block align-center logo" alt="">
    KilpiKuoret</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">&#9776;</button>
  <div class="collapse navbar-collapse" id="exCollapsingNavbar">
    <ul class="nav navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="ml-2" href="/tuoteryhmayllapito">Tuoteryhmien ylläpito</a>
        <a class="ml-2" href="/tuoteyllapito">Tuotteiden ylläpito</a>
        <a class="ml-2" href="/tilausyllapito">Tilausten käsittely</a>
      </li>
    </ul>
    <!-- TÄSSÄ TUOTERYHMÄ HAKUKENTTÄ -->
      <ul class="nav navbar-nav justify-content-between ml-auto">
      <!-- TÄSTÄ ALKAA LOGIN PAINIKKEEN MÄÄRITYKSET -->
      <li class="dropdown order-1">
        <button class="btn btn-outline-secondary float-right"><a href="/Kirjautuminen/Uloskirjaudu">Kirjaudu ulos</a></button>
      </li>
      </ul>
  </div>
</nav>

  <div class="container tausta">