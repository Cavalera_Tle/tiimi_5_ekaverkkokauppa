<footer class="fixed-bottom rounded-bottom navigointipalkki container page-footer">
    <footer class="container page-footer font-small indigo">
        <div class="row">
            <div class="col-12 mb-1 mt-1 text-center d-sm-none d-md-block d-none d-sm-block">
                <a class="mr-5  footerintekstit text-muted" href="/tietoameista">Tietoa meistä</a>
                <a class="mr-5  footerintekstit text-muted" href="/yhteystiedot">Yhteystiedot</a>
                <a class="mr-5  footerintekstit text-muted" href="/toimitusehdot">Toimitusehdot</a>
            </div>
            <div class="col-12 text-center text-muted">
                <p>Atso Vääräniemi - Elise Laurila - Joel Juusola - Joni Rainio - Maarit Kokko - Tomi Lepistö</p>
                <p class="copyright">© 2020 Copyright <img src="<?= base_url() ?>/img/kilpilogo.png" class="d-inline-block align-center kilpilogo" alt=""></p>
            </div>

        </div>
    </footer>
</footer>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/de67a128cc.js" crossorigin="anonymous"></script>

    </body>

    </html>