<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>KilpiKuoret</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="<?= base_url('/css/style.css'); ?>">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <style>
    body {
      background-image: url('https://images.unsplash.com/photo-1535157412991-2ef801c1748b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2000&q=80');
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
      background-attachment: fixed;
    }
  </style>
</head>

<body class="sisalto">

  <!--TÄSTÄ ALKAA NAVBAR -->
  <nav class="container sticky-top navbar navbar-expand-lg rounded-top navigointipalkki">
  <a class="navbar-brand mt-1 ml-2" href="/etusivu"> <img src="<?=base_url()?>/img/logo.png" class="d-inline-block align-center logo" alt="">
    KilpiKuoret</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">&#9776;</button>
  <div class="collapse navbar-collapse" id="exCollapsingNavbar">

    <ul class="navbar-nav mr-auto">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle2" href="/tuoteryhma" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Tuoteryhmät
        </a>
        <div class="dropdown-menu mt-4" aria-labelledby="navbarDropdown">
        <?php foreach($tuoteryhmat as $tuoteryhma): ?>
        <p class="ml-3"><?= anchor('/tuote/selaaTuotteita/' . $tuoteryhma->id, $tuoteryhma->nimi,'class="mr-5 footerintekstit text-muted"');?></p>   
    <?php endforeach ?>
        </div>
      </li>
      <li class="nav-item dropdown d-block d-sm-none">
      <a class="mr-5  footerintekstit text-muted" href="/tietoameista">Tietoa meistä</a> <br>
      <a class="mr-5  footerintekstit text-muted" href="/yhteystiedot">Yhteystiedot</a><br>
      <a class="mr-5  footerintekstit text-muted" href="/toimitusehdot">Toimitusehdot</a><br>
      </li>
    </ul>
    <div class="dropdown order-1">
      <!-- TÄSSÄ TUOTERYHMÄ HAKUKENTTÄ -->
      <ul class="nav navbar-nav justify-content-between ml-auto">
        <form class="form-inline my-2 my-lg-0" action="/EtsiTuoteryhmista/etsi" method="post">
          <div class="form-group has-search mr-1" style="width: 250px">
            <span class="fa fa-search form-control-feedback"></span>
            <input type="text" name="haku" class="form-control" placeholder="Etsi tuoteryhmistä" aria-label="etsi tuoteryhmistä">
          </div>
        </form>
          <!-- TÄSTÄ ALKAA LOGIN PAINIKKEEN MÄÄRITYKSET -->
          <li class="dropdown order-1">
            <button type="button" id="dropdownMenu1" data-toggle="dropdown" class="btn btn-outline-secondary dropdown-toggle">Kirjaudu <span class="caret"></span></button>
            <ul class="dropdown-menu dropdown-menu-right mt-4" style="max-width: 300px">
              <li class="px-3 py-2">
                <form class="form" action="/kirjautuminen/tarkista" method="POST">
                <div class="validointi">
                  <?= \Config\Services::validation()->listErrors(); ?>
                  </div>
                  <div class="form-group">
                    <input class="form-control form-control-sm" name="kayttaja" placeholder="Anna käyttäjänimi" type="text" maxlength="30" required="">
                  </div>
                  <div class="form-group">
                    <input class="form-control form-control-sm" name="salasana" type="password" placeholder="Anna salasana" maxlength="30" required="">
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-sm loginnappi float-right">Kirjaudu</button>
                  </div>
                  <div class="form-group text-center">
                    <small><?= anchor('rekisteroidy', 'Rekisteröidy') ?></small>
                  </div>
                </form>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <!-- TÄSTÄ ALKAA OSTOSKORI -->
    <div class="kori">
      <a class="nav-link text-center float-right" href="/ostoskori"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
        <?php print "<div class='float-right'><p id='ostoskori'>" . count($_SESSION['kori']) . "</p></div>";?>
      </a>
    </div>
    </div>
  </nav>
  <div class="container tausta">
    