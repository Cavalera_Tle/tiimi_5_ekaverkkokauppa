<div class="row content_background">
  <h1 class="col-12 text-center"><?= $title ?></h1>
  <?php foreach ($tuotteet as $tuote) : ?>
    <div class="kortit col-lg-3 col-md-4 col-sm-6">
      <form method="post" action="<?= site_url('ostoskori/lisaa/' . $tuote['id']); ?>">
        <div class="p-2 card my-3 text-center kortti">
          <img src="<?= base_url() ?>/img/<?= $tuote['kuva'] ?>" class="img-responsive card-img-top" alt="<?= $tuote['kuva'] ?>">
          <div class="card-body">
            <h5 class="card-title"><?= $tuote['nimi'] ?></h5>
            <p class="card-text"><?= $tuote['kuvaus'] ?></p>
            <p class="card-text"><small class="text-muted">Hinta: <?= $tuote['hinta'] ?>€</small></br>Varastossa: <?= $tuote['varastomaara'] ?>kpl</small></p>
            <div class="sticy-bottom">
              <button class="btn btn-secondary ostanappi btn-group">Osta</button>
      </form>
      <form method="post" action="<?= 'wishlist/remove/' . $tuote['id']; ?>">
        <button class="btn btn-secondary poistanappi btn-group mt-1 btn-sm">Poista</button>
      </form>
    </div>
</div>
</div>
</div>
<?php endforeach ?>
