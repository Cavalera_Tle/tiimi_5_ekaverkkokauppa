<?php
return[
    'nimi' => 'Nimesi',
    'sposti'=>'Sähköpostiosoitteesi',
    'otsikko'=>'Otsikko',
    'viesti'=>'Viesti',
    'laheta'=>'Lähetä', 
    'myymala'=>'Pentti Kaiteran katu 1, 90570 Oulu',
    'maa'=>'Suomi Finland',
    'myymalapnro'=>'+358 40 014 7478',
    'auki'=>'Ma - PE, 8:00-18:00',   
    'infosposti'=>'info@kilpikuoret.com',
    'salesposti'=>'sale@kilpikuoret.com',
    ''=>'',
    ''=>'',
];