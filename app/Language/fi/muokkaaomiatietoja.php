<?php
return[
    'kayttaja' => 'Käyttäjänimi',
    'enimi'=>'Etunimi',
    'snimi'=>'Sukunimi',
    'losoite'=>'Lähiosoite',
    'pnro'=>'Postinumero',
    'ptmp'=>'Postitoimipaikka' ,  
    'sposti'=>'Sähköpostiosoite',
    'puhnro'=>'Puhelinnumero',
    'ss'=>'Salasana',
    'ssu'=>'Vahvista salasana',
    'uk'=>'Haluatko tilata uutiskirjeemme?',
    'k'=>'Kyllä',
    'e'=>'Ei',
    'lupa'=>'Saako tietojasi luovuttaa kolmansille osapuolille?',
    'tallenna'=>'Tallenna',
    'etusivu'=>'Takaisin etusivulle',

];