drop database if exists kilpikuoret;
create database kilpikuoret;
use kilpikuoret;

create table asiakas (
  id int primary key auto_increment,
  etunimi varchar(50) not null,
  sukunimi varchar(100) not null,
  lahiosoite varchar(100),
  postinumero char(5),
  postitoimipaikka varchar(100),
  email varchar(255),
  puhelin varchar(20),
  kayttaja_id int
);
create table kayttaja (
  id int primary key auto_increment,
  kayttaja VARCHAR(30) NOT NULL UNIQUE,
  salasana VARCHAR(255) NOT NULL,
  etunimi varchar(50) not null,
  sukunimi varchar(100) not null,
  lahiosoite varchar(100),
  postinumero char(5),
  postitoimipaikka varchar(100),
  email varchar(255),
  puhelin varchar(20),
  uutiskirje boolean NOT NULL,
  tietojenluovutus boolean NOT NULL,
  rooli enum ('asiakas','yllapitaja') NOT NULL
);

create table tuoteryhma (
  id int primary key auto_increment,
  nimi varchar(255) not null unique
);

create table tuote (
  id int primary key auto_increment,
  nimi varchar(255) not null,
  hinta decimal(5,2) not null,
  kuvaus text,
  varastomaara int not null,
  kuva varchar(50),
  tuoteryhma_id int not null,
  index (tuoteryhma_id),
  foreign key (tuoteryhma_id) references tuoteryhma(id)
  on delete restrict
);


create table tilaus (
  id int primary key auto_increment,
  tilattu timestamp default current_timestamp,
  tila enum ('tilattu','toimitettu'),
  asiakas_id int not null,
  index (asiakas_id),
  foreign key (asiakas_id) references asiakas(id)
  on delete restrict
);

create table tilausrivi (
  tilaus_id int not null,
  index (tilaus_id),
  foreign key (tilaus_id) references tilaus(id)
  on delete restrict,
  tuote_id int not null,
  index (tuote_id),
  foreign key (tuote_id) references tuote(id)
  on delete restrict,
  maara smallint
);

create table palaute (
  nimi varchar(250) NOT NULL,
  sposti varchar(250) NOT NULL,
  otsikko varchar(100) NOT NULL,
  kuvaus varchar(500) NOT NULL
);

create table wishlist (
  kayttaja_id int not null,
  index (kayttaja_id),
  foreign key (kayttaja_id) references kayttaja(id)
  on delete restrict,
  tuote_id int not null,
  index (tuote_id),
  foreign key (tuote_id) references tuote(id)
  on delete restrict
);

insert into tuoteryhma (id, nimi) values (1, 'Puhelimet');
insert into tuoteryhma (id, nimi) values (2, 'Kuoret');
insert into tuoteryhma (id, nimi) values (3, 'Laturit');
insert into tuoteryhma (id, nimi) values (4, 'Pidikkeet');
insert into tuoteryhma (id, nimi) values (5, 'Extrat');


insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Samsung Galaxy A50','Android-puhelin, musta 4/128 Gt Dual-SIM',270,'samsung_luuri.png',15,1);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Nokia 6.2','Android-puhelin, 64 Gt, Dual-SIM, musta',250,'nokia_luuri.png',33,1);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Honor 20','Android-puhelin, 128 Gt Midnight Black',350,'honor_luuri.png',11,1);




insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Samsung Galaxy A50','Wave BookCase, musta suojakotelo',20,'samsung_kuori.png',30,2);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Nokia','CP-162-172 Flip Cover, musta suojakotelo',30,'nokia_kuori.png',20,2);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Honor',' Honor 20 Pro Phone Case, musta suojakuori',25,'honor_kuori.png',20,2);




insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Wave QiPad Duo','langaton latausalusta',60,'qipad_laturi.png',15,3);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Samsung Fast Charge','pikalaturi, Type-C -kaapelilla, musta',25,'fastcharge_laturi.png',32,3);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('TinyCharge 2','USB-seinälaturi, 12 W, musta',10,'tinycharge_laturi.png',27,3);




insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Macally MVENTGRAVITY','puhelinteline ilmastointiritilään',25,'macallygravity_pidike.png',14,4);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Macally Bikeholder','polkupyöräteline',8,'macallybike_pidike.png',45,4);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('MyFoneKit Windshield','autoteline joutsenkaulalla',20,'myfonekit_pidike.png',18,4);




insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('PopSockets','PopClip-teline, musta',13,'popsocket_extra.png',14,5);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Wave','kosketusnäyttösormikkaat, musta',10,'sormikkaat_extra.png',22,5);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Fuj:tech 2-in-1 Stylus','kosketusnäyttökynä ja kuulakärkikynä',4,'kyna.png',3,5);