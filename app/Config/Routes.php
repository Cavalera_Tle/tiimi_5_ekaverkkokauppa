<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
//Etusivu
$routes->get('/', 'Etusivu::index');
$routes->get('etusivu', 'Etusivu::index');
$routes->get('etusivu/index', 'Etusivu::index');
$routes->get('tietoameista', 'Etusivu::tietoameista');
$routes->get('toimitusehdot', 'Etusivu::toimitusehdot');
$routes->get('yhteystiedot', 'Etusivu::yhteystiedot');
// Tuotteiden selaus
$routes->get('selaatuotteita(:segment)', 'Tuote::selaaTuotteita/$1');
// Wishlist
$routes->get('wishlist', 'Wishlist::index');
// Ostoskori
$routes->get('ostoskori', 'Ostoskori::index');
$routes->get('ostoskori/lisaa(:segment)', 'Ostoskori::lisaa/$1');
$routes->get('tyhjenna', 'Ostoskori::tyhjenna');
// Kirjautuminen
$routes->get('UlosKirjaudu', 'Kirjautuminen::uloskirjaudu');
$routes->get('kirjautuminen', 'Kirjautuminen::index');
$routes->get('rekisteroidy', 'Rekisteroidy::rekisterointi');
//Ylläpidon sivut
$routes->get('yllapito', 'YllapitoEtusivu::index');
$routes->get('tilausyllapito', 'TilausYllapito::index');
$routes->get('tuoteryhmayllapito', 'TuoteryhmaYllapito::index');
$routes->get('tuoteyllapito', 'Tuoteyllapito::index');
$routes->get('tuoteyllapito/index', 'Tuoteyllapito::index');
$routes->get('muokkaaomiatietoja', 'MuokkaaOmiaTietoja::index');
$routes->get('muokkaaomiatietoja/update', 'MuokkaaOmiaTietoja::update');
// Tilaaminen
$routes->get('tilaus','Tilaus::index');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
