<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\TuoteRyhmaYllapitoModel;
use App\Models\TuoteYllapitoModel;
use App\Models\TuoteModel;

const KIRJAUTUMINEN_OTSIKKO = 'Kirjaudu verkkokauppaan';

class TuoteYllapito extends BaseController {

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();  
    } 
    
    public function index() {
        // Luodaan modelit
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        $tuote_model = new TuoteYllapitoModel;

        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        $data['tuotteet'] = $tuote_model->haeKaikki();

        if (isset($_SESSION['yllapitaja'])) {            //tämä käsky tarkistaa onko asiakas kirjautunut sivulle      
            $data['title'] = "Tervetuloa verkkokauppaan!";
    
            echo view('templates/yllapito_header');
            echo view('yllapito/tuote_yllapito_view',$data);
            echo view('templates/yllapito_footer');   
        } else {
            $data['title'] = KIRJAUTUMINEN_OTSIKKO;

            echo view('templates/header',$data);
            echo view('/kirjautuminen/kirjaudu',$data);
            echo view('templates/footer');
        }
    }

    public function update($id) {
        // Luodaan model
        $tuote_model = new TuoteModel;

        $data['tuotteet'] = $tuote_model->where('id', $id)->findAll();
        
        echo view('templates/yllapito_header');
        echo view('yllapito/tuote_muokkaus_view', $data);
        echo view('templates/yllapito_footer');
    }

    public function delete($id) {
        //Luodaan uusi model
        $tuote_model = new TuoteYllapitoModel;
        
        $tuote_model->remove($id);

        // Ohjataan takaisin tuotteiden hallintasivulle
        return redirect('tuoteyllapito');
    }

    public function muokkaa() {
        //Luodaan model
        $tuote_model = new TuoteYllapitoModel;      
        $kuva = $this->request->getVar('kuva');
        $id = $this->request->getVar('id');
        $tuoteNimi = $this->request->getVar('tuotenimi');
        $hinta = $this->request->getVar('hinta');
        $kuvaus = $this->request->getVar('kuvaus');
        $varastomaara = $this->request->getVar('varastomaara');
        $tuoteryhma_id = $this->request->getVar('tuoteryhma_id');

        //Käytetään modelin funktiota paivitaTuote 
        $tuote_model->paivitaTuote($id, $tuoteNimi, $hinta, $kuvaus, $varastomaara, $kuva, $tuoteryhma_id);

        // Ohjataan takaisin tuotteiden hallintasivulle
        return redirect('tuoteyllapito');

    }
    public function lisaa() {
        // Luodaan model
        $tuote_model = new TuoteYllapitoModel;
        $data['tuotteet'] = $tuote_model->haeKaikki();

        if(!$this->validate([
            'kuva' => [
                'uploaded[kuva]',
                'mime_in[kuva,image/jpg,image/jpeg,image/gif,image/png]',
                'max_size[kuva,4096]',
            ]
        ])){
            echo view('templates/yllapito_header');
            echo view('yllapito/tuote_yllapito_view', $data);
            echo view('templates/yllapito_footer');
        }
        else {      
            $kuva = $this->request->getFile('kuva');
            $polku = APPPATH;
            $polku = str_replace('app','public/img',$polku);
            $kuva->move($polku);
            $kuva = $kuva->getName();

            $tuoteNimi = $this->request->getVar('tuotenimi'); 
            $hinta = $this->request->getVar('hinta');
            $kuvaus = $this->request->getVar('kuvaus');
            $varastomaara = $this->request->getVar('varastomaara');                
            $tuoteryhma_id = $this->request->getVar('tuoteryhma_id');        

            $tuote_model->lisaaTuote($tuoteNimi, $hinta, $kuvaus, $varastomaara, $kuva, $tuoteryhma_id);
            // Ohjataan takaisin tuotteiden hallintasivulle
            return redirect('tuoteyllapito');
        }
    }
}
?>