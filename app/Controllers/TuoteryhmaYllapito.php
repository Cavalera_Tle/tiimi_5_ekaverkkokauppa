<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\TuoteryhmaYllapitoModel;

const KIRJAUTUMINEN_OTSIKKO = 'Kirjaudu verkkokauppaan';

class TuoteryhmaYllapito extends BaseController {   

    // Funktio luo istuntomuuttujan
    public function __construct()    {
        $session = \Config\Services::session();
        $session->start();  
    }

    // Funktio tarkistaa kirjautumisen ja kutsuu näkymää
    public function index() {
        // Luodaan model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;

        // Tarkistetaan onko asiakas kirjautunut sivulle
        if (isset($_SESSION['yllapitaja'])) {    
            // Lisätään taulukkoon kaikkien tuoteryhmien tiedot käyttäen modelin funktiota haeKaikki()
            $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();

            // Tulostaa näkymät
            echo view('templates/yllapito_header');
            echo view('yllapito/tuoteryhma_yllapito_view',$data);
            echo view('templates/yllapito_footer');    
        } 
        // Palauttaa käyttäjän kirjautumiseen
        else {
            return redirect('kirjautuminen');
        }
    }

    // Funktio luo uuden tuoteryhmän
    public function lisaa() {
         // Luodaan uusi TuoteryhmaYllapitoModel
         $tuoteryhma_model = new TuoteryhmaYllapitoModel;
         
        // Haetaan muuttujaan arvo, joka on lähetetty formilla
        $tuoteryhmanNimi = $this->request->getVar('tuoteryhma');

        // Kutsutaan modelin funktiota lisaaTuoteryhma, johon viedään muuttuja ja tallennetaan se tietokantaan
        $tuoteryhma_model->lisaaTuoteryhma($tuoteryhmanNimi);

         // Palauttaa sivulle
         return redirect('tuoteryhmayllapito');
    }

    // Funktio poistaa tuoteryhmän
    public function delete($id) {
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
             
        // Poistetaan tuoteryhmä modelin remove funktiolla
        $poisto = $tuoteryhma_model->remove($id);

        if ($poisto == FALSE) {
            print("Et voi poistaa tuoteryhmä', koska se sisältää tuotteita.");
        }
        // Palauttaa sivulle
        return redirect('tuoteryhmayllapito');
    }

    // Funktio ottaa muuttujat, jotka on parametreina ja lähettää ne näkymään
    public function update($id) {
        // Luodaan model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;

        $data['tuoteryhmat'] = $tuoteryhma_model->haeTuoteryhma($id);

        // Tulostaa näkymät
        echo view('templates/yllapito_header');
        echo view('yllapito/tuoteryhma_muokkaus_view', $data);
        echo view('templates/yllapito_footer');
       
    }

    // Funktio päivittää tuoteryhmän 
    public function muokkaa() {
        // Luodaan uusi model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;

        // Haetaan muuttujiin arvot, jotka on lähetetty formilla
        $id = $this->request->getVar('id');
        $tuoteryhmanNimi = $this->request->getVar('tuoteryhma');

        //Käytetään modelin funktiota paivitaTuoteryhma
        $tuoteryhma_model->paivitaTuoteryhma($id, $tuoteryhmanNimi);

        // Palauttaa sivulle
        return redirect('tuoteryhmayllapito');
    }
}
?>