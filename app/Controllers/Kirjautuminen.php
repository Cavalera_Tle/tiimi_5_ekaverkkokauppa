<?php namespace App\Controllers;

use App\Models\KirjautuminenModel;
use App\Models\YllapitoKirjautuminenModel;
use App\Models\TuoteryhmaYllapitoModel;

const KIRJAUTUMINEN_OTSIKKO = 'Kirjaudu verkkokauppaan';
const REKISTEROITYMINEN_OTSIKKO = 'Rekisteröidy verkkokauppaan';

class Kirjautuminen extends BaseController {

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();  
    }
    
    public function index() {
        // Luodaan model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel; 

        // Otsikko
        $data['title'] = KIRJAUTUMINEN_OTSIKKO;

        // Headerille tuoteryhmien tiedot
        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();    

        // Ladataan näkymät kirjautumisen mukaan
        if (isset($_SESSION['kayttaja'])) {
           return redirect('etusivu');   
        } else if (isset($_SESSION['yllapitaja'])) {
           return redirect('yllapito');   
        } else {
            echo view('templates/header', $data);
            echo view('kirjautuminen/kirjaudu',$data);
            echo view('templates/footer');
        }
    }

    public function tarkista() {
        
        // Luodaan modelit
        $tuoteryhma_model = new TuoteryhmaYllapitoModel; 
        $model = new KirjautuminenModel();
        $model2 = new YllapitoKirjautuminenModel();

        $data['title'] = KIRJAUTUMINEN_OTSIKKO;
        // Headerille tuoteryhmien tiedot
        $data2['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();    
        
        if(!$this->validate([
            'kayttaja' => 'required|min_length[8]|max_length[30]',
            'salasana' => 'required|min_length[8]|max_length[30]',
        ])) {
            return redirect('kirjautuminen');
        } else {
            $yllapitaja = $model2->check(
                $this->request->getVar('kayttaja'),
                $this->request->getVar('salasana'),
            );           
            if ($yllapitaja) {
                $_SESSION['yllapitaja'] = $yllapitaja;
                return redirect('yllapito');
            } else {
                $kayttaja = $model->check(
                    $this->request->getVar('kayttaja'),
                    $this->request->getVar('salasana'),
                );
            }
            if($kayttaja) {
                    $_SESSION['kayttaja'] = $kayttaja;
                    return redirect('etusivu');
            } else {
                echo view('templates/header', $data2);
                echo view('/kirjautuminen/kirjaudu',$data);
                echo view('templates/footer');
                echo "<script> $('#vaaraTunnusTaiSs').modal('show') </script>";
            }
        }
    }

    public function uloskirjaudu() {
        // Luodaan model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;

        // Headerille tuoteryhmien tiedot
        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();

        // Tyhjennetään istuntuomuuttujat
        unset($_SESSION['kayttaja']);        
        unset($_SESSION['yllapitaja']);
        $_SESSION['kori'] = array();

        // Ladataan näkymät
        echo view('templates/header', $data);
        echo view('etusivu_view');
        echo view('templates/footer');
        //return redirect()->to( base_url('etusivu'));
        echo "<script> $('#UlosKirjaudu').modal('show') </script>";
    }
}
?>