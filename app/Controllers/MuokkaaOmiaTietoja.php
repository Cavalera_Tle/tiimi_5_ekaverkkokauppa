<?php namespace App\Controllers;

use App\Models\AsiakasTietoMuokkausModel;
use App\Models\TuoteryhmaYllapitoModel;

const MuokkausOtsikko = 'Asiakastietojen muokkaus';
const KIRJAUTUMINEN_OTSIKKO = 'Kirjaudu verkkokauppaan';

class MuokkaaOmiaTietoja extends BaseController {

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();  

        if (!isset($_SESSION['kori'])) {
            $_SESSION['kori'] = array();
        } 
    } 
        
    public function index() {
        // Luodaan modelit
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        $asiakasTiedot = new AsiakasTietoMuokkausModel();

        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        $data['asiakastiedot'] = $asiakasTiedot->haeTiedot();

        $data['title'] = MuokkausOtsikko;

        if (isset($_SESSION['kayttaja'])) {
            echo view('templates/kirjautuneen_header',$data);
            echo view('muokkaaomiatietoja_view',$data);
            echo view('templates/kirjautuneen_footer'); 
        } else {
            return redirect('kirjautuminen');
        }
    }

    public function update() {
        // Luodaan model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;

        $kayttaja = $_SESSION['kayttaja'];

        $data2['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        $data['title'] = MuokkausOtsikko;

        // Haetaan kirjautuneen käyttäjän tiedot
        $data['id'] = $kayttaja->id;
        $data['kayttaja'] = $kayttaja->kayttaja;
        $data['enimi'] = $kayttaja->etunimi;
        $data['snimi'] = $kayttaja->sukunimi;
        $data['lahiosoite'] = $kayttaja->lahiosoite;
        $data['postinumero'] = $kayttaja->postinumero;
        $data['postitoimipaikka'] = $kayttaja->postitoimipaikka;
        $data['email'] = $kayttaja->email;
        $data['puhelin'] = $kayttaja->puhelin;
        $data['uutiskirje'] = $kayttaja->uutiskirje;
        $data['tietojenluovutus'] = $kayttaja->tietojenluovutus;
        
        // Ladataan näkymä
        echo view('templates/kirjautuneen_header',$data2);
        echo view('muokkaaomiatietoja_view',$data);
        echo view('templates/kirjautuneen_footer');
    }

    public function muokkaa() {
        // Luodaan modelit
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        $asiakasTiedot = new AsiakasTietoMuokkausModel;

        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        $data['title'] = MuokkausOtsikko;

        $id = $this->request->getVar('id');
        $kayttaja = $this->request->getVar('kayttaja');
        $salasana = password_hash($this->request->getVar('salasana'),PASSWORD_DEFAULT);
        $etunimi = $this->request->getVar('enimi');
        $sukunimi = $this->request->getVar('snimi');
        $lahiosoite = $this->request->getVar('lahiosoite');
        $postinumero = $this->request->getVar('postinumero');
        $postitoimipaikka = $this->request->getVar('postitoimipaikka');
        $sahkoposti = $this->request->getVar('email');
        $puhelin = $this->request->getVar('puhelin');
        $uutiskirje = $this->request->getVar('uutiskirje');
        $tietojenluovutus = $this->request->getVar('tietojenluovutus');

        if (!$this->validate([            
            'kayttaja' => 'required|min_length[8]|max_length[30]',
            'email' => 'valid_email|required',
            'salasana' => 'required|min_length[8]|max_length[30]',
            'confirmpassword' => 'required|min_length[8]|max_length[30]|matches[salasana]',
        ])) {
            $kayttaja = $_SESSION['kayttaja'];

            $data['id'] = $kayttaja->id;
            $data['kayttaja'] = $kayttaja->kayttaja;
            $data['enimi'] = $kayttaja->etunimi;
            $data['snimi'] = $kayttaja->sukunimi;
            $data['lahiosoite'] = $kayttaja->lahiosoite;
            $data['postinumero'] = $kayttaja->postinumero;
            $data['postitoimipaikka'] = $kayttaja->postitoimipaikka;
            $data['email'] = $kayttaja->email;
            $data['puhelin'] = $kayttaja->puhelin;
            $data['uutiskirje'] = $kayttaja->uutiskirje;
            $data['tietojenluovutus'] = $kayttaja->tietojenluovutus;

            echo view('templates/kirjautuneen_header', $data);
            echo view('muokkaaomiatietoja_view',$data);
            echo view('templates/kirjautuneen_footer');
        } else {
            //Käytetään modelin funktiota paivitaTiedot 
            $_SESSION['kayttaja'] = $asiakasTiedot->paivitaTiedot($id, $kayttaja, $salasana, $etunimi, $sukunimi, $lahiosoite, $postinumero, $postitoimipaikka, $sahkoposti, $puhelin, $uutiskirje, $tietojenluovutus); 
            echo view('templates/header',$data);
            echo view('/kirjautuminen/kirjaudu');
            echo view('templates/footer');
           // unset($_SESSION['kayttaja']);
            echo "<script> $('#tietojenmuokkaus').modal('show') </script>";
        }
    }
    }
?>

 