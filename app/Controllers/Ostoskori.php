<?php namespace App\Controllers;

use App\Models\TuoteryhmaYllapitoModel;
use App\Models\TuoteModel;

class Ostoskori extends BaseController {    

    // Funktio luo istuntomuuttujan
    public function __construct() {
        $session = \Config\Services::session();
        $session->start();

        //Tarkistetaan onko sessioon tallennettu muuttuja ostoskori, jos ei luodaan se
        if (!isset($_SESSION['kori'])) {
            $_SESSION['kori'] = array();
        }
    }

    // Funktio hakee ostoskorin ja kutsuu näkymää
    public function index() {
        // Luodaan modelit
        $tuoteModel = new TuoteModel();
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;

        $data2['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        
        // Haetaan $ostokset-muuttujaan ostoskorin sisältö
        if (count($_SESSION['kori']) > 0) {
            $ostokset = $tuoteModel->haeTuotteet($_SESSION['kori']);
        } 
        // Tai luodaan muuttujaan tyhjä taulukko
        else {
            $ostokset = array();
        }
        // Luodaan taulukko, johon lisätään ostoskorin sisältö muuttujasta
        $data['ostokset'] = $ostokset;
        
        // Ladataan näkymä sen mukaan onko käyttäjä kirjautunut vai ei
        if (isset($_SESSION['kayttaja'])) {
            echo view('templates/kirjautuneen_header',$data2);
            echo view('ostoskori_view', $data);
            echo view('templates/kirjautuneen_footer');
        } else {
            echo view('templates/header',$data2);
            echo view('ostoskori_view', $data);
            echo view('templates/footer');
        }
    }

    public function lisaa($tuote_id) {
        array_push($_SESSION['kori'],$tuote_id);

        // Palauttaa ostoskorinäkymään
        return redirect('ostoskori');
    }

    public function vahenna($tuote_id) {
        $index = -1;
        for ($i=0; $i < count($_SESSION['kori']);$i++) {
            if ($_SESSION['kori'][$i] === $tuote_id) {
                $index = $i;
            }
        }    
        array_splice($_SESSION['kori'],$index, 1);

        // Palauttaa ostoskorinäkymään
        return redirect('ostoskori');
    }

    public function tyhjenna() {
        $_SESSION['kori'] = null;
        
        // Palauttaa ostoskorinäkymään
        return redirect('ostoskori');
    }
}
?>