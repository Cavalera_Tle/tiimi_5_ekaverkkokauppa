<?php namespace App\Controllers;

use App\Models\KirjautuminenModel;
use App\Models\TuoteryhmaYllapitoModel;

const REKISTEROITYMINEN_OTSIKKO = 'Rekisteröidy verkkokauppaan';
const KIRJAUTUMINEN_OTSIKKO = 'Kirjaudu verkkokauppaan';

class Rekisteroidy extends BaseController {

    public function __construct()    {
        $session = \Config\Services::session();
        $session->start();
        // Tarkistetaan onko sessioon tallennettu muuttuja nimeltä ostoskori, jos ei luodaan se  
        if (!isset($_SESSION['kori'])) {
            $_SESSION['kori'] = array();
        } 
    } 
    
    public function index() {
        // Luodaan model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        
        // Headerille tuoteryhmien tiedot
        $data2['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        
        // Ladataan näkymät kirjautumisen mukaan
        if (isset($_SESSION['kayttaja'])) {    
            echo view('templates/kirjautuneen_header', $data2);
            echo view('etusivu_view');
            echo view('templates/kirjautuneen_footer');    
        } else if (isset($_SESSION['yllapitaja'])) {
            echo view('templates/yllapito_header');
            echo view('yllapitoetusivu_view');
            echo view('templates/yllapito_footer');    
        } else {
            $data['title'] = REKISTEROITYMINEN_OTSIKKO;
            echo view('templates/header', $data2);
            echo view('kirjautuminen/kirjaudu',$data);
            echo view('templates/footer');
        }
    }
    
    public function rekisteroidy() {
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        $data2['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        $data['title'] = REKISTEROITYMINEN_OTSIKKO;

        echo view('templates/header',$data2);
        echo view('kirjautuminen/rekisteroidy',$data);
        echo view('templates/footer',$data);
    }
    
    public function rekisterointi() {
        // Luodaan modelit
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        $model = new KirjautuminenModel();
        
        $data2['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        $data['title'] = REKISTEROITYMINEN_OTSIKKO;
        $data3['title'] = KIRJAUTUMINEN_OTSIKKO;

        if (!$this->validate([
            'kayttaja' => 'required|min_length[8]|max_length[30]',
            'email' => 'valid_email|required',
            'salasana' => 'required|min_length[8]|max_length[30]',
            'confirmpassword' => 'required|min_length[8]|max_length[30]|matches[salasana]',
        ])){
            echo view('templates/header', $data2);
            echo view('kirjautuminen/rekisteroidy',$data);
            echo view('templates/footer');
        }
        else {
            $model->save([
                'kayttaja' => $this->request->getVar('kayttaja'),
                'salasana' => password_hash($this->request->getVar('salasana'),PASSWORD_DEFAULT),
                'etunimi' => $this->request->getVar('enimi'),
                'sukunimi' => $this->request->getVar('snimi'),
                'lahiosoite' => $this->request->getVar('lahiosoite'),
                'postinumero' => $this->request->getVar('postinumero'),
                'postitoimipaikka' => $this->request->getVar('postitoimipaikka'),
                'email' => $this->request->getVar('email'),
                'puhelin'=>$this->request->getVar('puhelin'),
                "uutiskirje"=>$this->request->getvar("uutiskirje"),
                "tietojenluovutus"=>$this->request->getvar("tietojenluovutus")
            ]);
            echo view('templates/header', $data2);
            echo view('kirjautuminen/kirjaudu',$data3);
            echo view('templates/footer');
            echo "<script> $('#Rekisteroityminen').modal('show') </script>";
        }
    }
}
?>