<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\TilausYllapitoModel;
use App\Models\TilausriviModel;

class TilausYllapito extends BaseController {   

    // Funktio luo istuntomuuttujan
    public function __construct()    {
        $session = \Config\Services::session();
        $session->start();  
    }

    public function index() {
        $tilaus_model = new TilausYllapitoModel;
        $tilausrivi_model = new TilausriviModel;

        $data['tilaukset'] = $tilaus_model->haeKaikki();
        $data['tilausrivit'] = $tilausrivi_model->haeKaikki();

        // Ladataan näkymä sen mukaan onko käyttäjä kirjautunut vai ei
        if (isset($_SESSION['yllapitaja'])) {
            echo view('templates/yllapito_header');
            echo view('yllapito/tilaus_yllapito_view',$data);
            echo view('templates/yllapito_footer');    
        } else {
            return redirect('kirjautuminen');
        }
    }

    public function update($id) {
        // Luodaan model
        $tilaus_model = new TilausYllapitoModel;

        $data['tilaukset'] = $tilaus_model->haeTilaus($id);

        // Tulostaa näkymät
        echo view('templates/yllapito_header');
        echo view('yllapito/tilaus_muokkaus_view', $data);
        echo view('templates/yllapito_footer');
    }

    // Funktio päivittää tilauksen tilan
    public function muokkaa() {
        // Luodaan model
        $tilaus_model = new TilausYllapitoModel;

        // Haetaan muuttujiin arvot, jotka on lähetetty formilla
        $id = $this->request->getVar('id');
        $tila = $this->request->getVar('tila');

        // Käytetään modelin funktiota paivitaTuoteryhma
        $tilaus_model->paivitaTila($id, $tila);

        // Palauttaa tilausnäkymään
        return redirect('tilausyllapito');
    }
}
?>