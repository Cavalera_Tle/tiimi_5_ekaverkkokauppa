<?php namespace App\Controllers;

use App\Models\TuoteryhmaYllapitoModel;
use App\Models\AsiakasTietoMuokkausModel;

class YllapitoEtusivu extends BaseController {

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();  
    } 
    
    public function index() {
        // Luodaan model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;     

        // Lisätään taulukkoon tuoteryhmien tiedot ja lähetetään näkymälle
        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki(); 

        //Tarkistetaan ylläpitäjä
        if (isset($_SESSION['yllapitaja'])) {
            echo view('templates/yllapito_header',$data);
            echo view('yllapito/yllapito_view',$data);
            echo view('templates/yllapito_footer');    
        } else {
            echo view('templates/header',$data);
            echo view('etusivu_view',$data);
            echo view('templates/footer');
       }
    }
}
?>