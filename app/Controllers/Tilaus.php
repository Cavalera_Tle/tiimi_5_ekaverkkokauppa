<?php namespace App\Controllers;

use App\Models\TuoteryhmaYllapitoModel;
use App\Models\TilausModel;
use App\Models\AsiakasTietoMuokkausModel;
use App\Models\TuoteModel;

const TilauslomakeOtsikko = 'Tilauslomake';

class Tilaus extends BaseController {

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();
    }

    public function index() {
        // Luodaan modelit
        $asiakasModel = new AsiakasTietoMuokkausModel;
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;

        $data2['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        $data['title'] = TilauslomakeOtsikko;
        // Jos ostoskori on tyhjä -->
        if (count($_SESSION['kori']) === 0) {
            return redirect ('ostoskori');
        // Jos ostoskorissa on tuotteita -->
        } else {
            if (isset($_SESSION['kayttaja'])) { 
                // Käyttäjän tiedot muuttujaan
                $kayttaja = $_SESSION['kayttaja'];
                // Haetaan asiakkaan id
                $asiakas_id = $kayttaja->id;
                
                // Haetaan asiakkaan tiedot
                $asiakas = $asiakasModel->haeAsiakas($asiakas_id);
                
                $data['asiakas'] = $asiakas;

                echo view('templates/kirjautuneen_header',$data2);
                echo view('tilaus/kayttajatilaus_view',$data);
                echo view('templates/kirjautuneen_footer');
            } else {
                echo view('templates/header', $data2);
                echo view('tilaus/vierastilaus_view',$data);
                echo view('templates/footer');
            }
        }
    }

    public function tilaaminen() {
        // Luodaan modelit
        $tilausModel = new Tilausmodel();
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        $tuoteModel = new TuoteModel();

        $data2['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        $data['title'] = "Tilauksen vahvistus";

        if (isset($_SESSION['kayttaja'])) {
            $asiakas = array([
                'id' => $this->request->getVar('id'),
                'kayttaja' => $this->request->getVar('kayttaja'),
                'etunimi' => $this->request->getVar('etunimi'),
                'sukunimi' => $this->request->getVar('sukunimi'),
                'lahiosoite' => $this->request->getVar('lahiosoite'),
                'postinumero' => $this->request->getVar('postinumero'),
                'postitoimipaikka' => $this->request->getVar('postitoimipaikka'),
                'email' => $this->request->getVar('email'),
                'puhelin' => $this->request->getVar('puhelin')
            ]);
            $_SESSION['asiakas'] =[
                'id' => $this->request->getVar('id'),
                'kayttaja' => $this->request->getVar('kayttaja'),
                'etunimi' => $this->request->getVar('etunimi'),
                'sukunimi' => $this->request->getVar('sukunimi'),
                'lahiosoite' => $this->request->getVar('lahiosoite'),
                'postinumero' => $this->request->getVar('postinumero'),
                'postitoimipaikka' => $this->request->getVar('postitoimipaikka'),
                'email' => $this->request->getVar('email'),
                'puhelin' => $this->request->getVar('puhelin')
            ];
            $ostokset = $tuoteModel->haeTuotteet($_SESSION['kori']);

            $data['asiakas'] = $asiakas;
            $data['ostokset'] = $ostokset;

            echo view('templates/kirjautuneen_header',$data2);
            echo view('tilaus/kayttajavahvistus_view',$data);
            echo view('templates/kirjautuneen_footer');
        } else {
            //tallentaa asiakkaan tiedot formilta
            $asiakas = array([
                'etunimi' => $this->request->getVar('etunimi'),
                'sukunimi' => $this->request->getVar('sukunimi'),
                'lahiosoite' => $this->request->getVar('lahiosoite'),
                'postinumero' => $this->request->getVar('postinumero'),
                'postitoimipaikka' => $this->request->getVar('postitoimipaikka'),
                'email' => $this->request->getVar('email'),
                'puhelin' => $this->request->getVar('puhelin')
            ]);
            $_SESSION['asiakas'] =[
                'etunimi' => $this->request->getVar('etunimi'),
                'sukunimi' => $this->request->getVar('sukunimi'),
                'lahiosoite' => $this->request->getVar('lahiosoite'),
                'postinumero' => $this->request->getVar('postinumero'),
                'postitoimipaikka' => $this->request->getVar('postitoimipaikka'),
                'email' => $this->request->getVar('email'),
                'puhelin' => $this->request->getVar('puhelin')
            ];

            $ostokset = $tuoteModel->haeTuotteet($_SESSION['kori']);

            $data['asiakas'] = $asiakas;
            $data['ostokset'] = $ostokset;

            echo view('templates/header',$data2);
            echo view('tilaus/vierasvahvistus_view',$data);
            echo view('templates/footer');
        }
    }
    
    public function tallennus() {
        // Luodaan modelit
        $tilausModel = new Tilausmodel();
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();

        if (isset($_SESSION['kayttaja'])) {
            $kayttaja = $_SESSION['asiakas'];

            $tilausModel->tilaus($kayttaja, $_SESSION['kori']);
  
            // tyhjennetään ostoskori ja asiakas
            $_SESSION['kori'] = array();
            unset($_SESSION['asiakas']);

            // Näytetään tilaustehty sivu
            echo view('templates/kirjautuneen_header',$data);
            echo view('tilaus/tilaustehty_view');
            echo view('templates/kirjautuneen_footer');
        } else {
            $asiakas = $_SESSION['asiakas'];
            $tilausModel->tallenna($asiakas, $_SESSION['kori']);

            // tyhjennetään ostoskori
            $_SESSION['kori'] = array();
            $_SESSION['asiakas'] = array();
            $asiakas = null;
            // Näytetään tilaustehty sivu
            echo view('templates/header',$data);
            echo view('tilaus/tilaustehty_view');
            echo view('templates/footer');
        }    
    }
}
?>