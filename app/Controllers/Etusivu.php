<?php namespace App\Controllers;

use App\Models\TuoteryhmaYllapitoModel;
use App\Models\PalauteModel;

class Etusivu extends BaseController {

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();  

        // Tarkistetaan onko sessioon tallennettu muuttuja nimeltä ostoskori, jos ei luodaan se
        if (!isset($_SESSION['kori'])) {
            $_SESSION['kori'] = array();
        } 
    } 
    
    public function index() {
       // Luodaan model
       $tuoteryhma_model = new TuoteryhmaYllapitoModel;

       // Headerille tuoteryhmien tiedot
       $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki(); 

        // Ladataan näkymät kirjautumisen mukaan
        if (isset($_SESSION['kayttaja'])) {
            echo view('templates/kirjautuneen_header', $data);
            echo view('etusivu_view',$data);
            echo view('templates/kirjautuneen_footer');    
        } else if (isset($_SESSION['yllapitaja'])) {
            echo view('templates/yllapito_header');
            echo view('yllapito_view');
            echo view('templates/yllapito_footer');    
        }else {
            echo view('templates/header',$data);
            echo view('etusivu_view',$data);
            echo view('templates/footer');
        }
    }

    public function tietoaMeista() {
        // Luodaan model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;

        // Headerille tuoteryhmien tiedot
        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();

        // Ladataan näkymät kirjautumisen mukaan
        if (isset($_SESSION['kayttaja'])) {
            echo view('templates/kirjautuneen_header', $data);
            echo view('tietoameista_view');
            echo view('templates/kirjautuneen_footer');    
        } else {
            echo view('templates/header',$data);
            echo view('tietoameista_view');
            echo view('templates/footer');
        }
    }

    public function toimitusehdot() {
        // Luodaan model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;

        // Headerille tuoteryhmien tiedot
        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        
        // Ladataan näkymät kirjautumisen mukaan
        if (isset($_SESSION['kayttaja'])) {
            echo view('templates/kirjautuneen_header',$data);
            echo view('toimitusehdot_view');
            echo view('templates/kirjautuneen_footer');    
        } else {  
            echo view('templates/header',$data);
            echo view('toimitusehdot_view');
            echo view('templates/footer');
        }
    }

    public function yhteystiedot() {
        // Luodaan model
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        // Headerille tuoteryhmien tiedot
        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        
        // Ladataan näkymät kirjautumisen mukaan
        if (isset($_SESSION['kayttaja'])) {
            echo view('templates/kirjautuneen_header',$data);
            echo view('yhteystiedot_view');
            echo view('templates/kirjautuneen_footer');    
        } else {
            echo view('templates/header',$data);
            echo view('yhteystiedot_view');
            echo view('templates/footer');
        }
    }

    public function palaute() {
        // Luodaan modelit
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        $model = new PalauteModel();

        // Headerille tuoteryhmien tiedot
        $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();

        // Tallennetaan palautelomake tietokantaan 
        $model->save([
            'nimi' => $this->request->getVar('form-name'),
            'sposti' => $this->request->getVar('form-email'),
            'otsikko' =>$this->request->getVar('form-subject'),
            'kuvaus' =>$this->request->getvar('form-text')
        ]);

        // Ladataan näkymät kirjautumisen mukaan
        if (isset($_SESSION['kayttaja'])) {
            echo view('templates/kirjautuneen_header',$data);
            echo view('yhteystiedot_view');
            echo view('templates/kirjautuneen_footer');  
            echo "<script> $('#kiitospalaute').modal('show') </script>";  
        } else {
            echo view('templates/header',$data);
            echo view('yhteystiedot_view');
            echo view('templates/footer');
            echo "<script> $('#kiitospalaute').modal('show') </script>";
        }
    }
}
?>