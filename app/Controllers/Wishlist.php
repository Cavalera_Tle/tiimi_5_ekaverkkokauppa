<?php namespace App\Controllers;

use App\Models\TuoteryhmaYllapitoModel;
use App\Models\WishlistModel;
use App\Models\TuoteModel;

class Wishlist extends BaseController {    

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();
    }

    public function index() {
        // Luodaan modelit
        $tuoteryhma_model = new TuoteryhmaYllapitoModel(); 
        $wishlist_model = new WishlistModel();
        $tuoteModel = new TuoteModel();

        if (isset($_SESSION['kayttaja'])) {
            // Haetaan kirjautuneen käyttäjän id 
            $kayttaja = $_SESSION['kayttaja'];
            $kayttaja_id = $kayttaja->id;

            // Haetaan tiedot 
            $wishlist = $wishlist_model->haeKaikki($kayttaja_id);

            // Muutetaan taulukkomuotoa
            $wishlist = json_decode( json_encode($wishlist), true);

            // Haetaan wishlistin tiedot taulukkoon
            $data['tuotteet'] = $tuoteModel->haeTuotteet($wishlist);

            // Lisätään taulukkoon tuoteryhmien ja otsikon tiedot ja lähetetään näkymälle
            $data['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();  
            $data['title'] = 'Wishlist';

            // Ladataan näkymät
            echo view('templates/kirjautuneen_header',$data);
            echo view('wishlist_view',$data);
            echo view('templates/kirjautuneen_footer');
        } else {
            return redirect('kirjautuminen');
        }
    }

    public function lisaa($tuote_id) {
        // Ladataan näkymä sen mukaan onko kirjautunut vai ei
        if (isset($_SESSION['kayttaja'])) {
            $kayttaja = $_SESSION['kayttaja'];
            $kayttaja_id = $kayttaja->id;

            // Luodaan model
            $wishlist_model = new WishlistModel;

            // Lisätään tuote wishlistiin
            $wishlist_model->lisaaTuote($kayttaja_id, $tuote_id);

            // Palataan wishlistiin
            return redirect('wishlist');
        } else {
            return redirect('kirjautuminen');
        }
    }

     // Funktio poistaa tuotteen wishlististä
      public function remove($id) {
        //Luodaan uusi model
        $wishlistModel = new WishlistModel;
        
        $wishlistModel->remove($id);

        return redirect('wishlist');
    }
}
?>