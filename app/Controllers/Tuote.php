<?php namespace App\Controllers;

use App\Models\TuoteModel;
use App\Models\TuoteryhmaYllapitoModel;

class Tuote extends BaseController {
    public function __construct() {
        $session = \Config\Services::session();
        $session->start();

        if (!isset($_SESSION['kori'])) {
            $_SESSION['kori'] = array();
        }
    }

    public function selaaTuotteita($tuoteryhmaId) {
        // Luodaan modelit
        $tuoteryhma_model = new TuoteryhmaYllapitoModel;
        $tuote_model = new TuoteModel;
       
        $data['title'] = $tuoteryhma_model->haeTuoteryhma($tuoteryhmaId);
        $data2['tuoteryhmat'] = $tuoteryhma_model->haeKaikki();
        $data['tuotteet'] = $tuote_model->where('tuoteryhma_id', $tuoteryhmaId)->findAll();
        
        // Ladataan näkymät kirjautumisen mukaan
        if (isset($_SESSION['kayttaja'])) {
            echo view('templates/kirjautuneen_header', $data2);
            echo view('selaaTuotteita_view', $data);
            echo view('templates/kirjautuneen_footer');
        } else {
            echo view('templates/header',$data2);
            echo view('selaaTuotteita_view', $data);
            echo view('templates/footer');
        }
    }
}
?>