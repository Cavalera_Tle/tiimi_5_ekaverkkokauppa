<?php namespace App\Models;

use CodeIgniter\Model;
use App\Models\AsiakasModel;
use App\Models\TilausriviModel;
use App\Models\AsiakasTietoMuokkausModel;

class TilausModel extends Model {
    protected $table ='tilaus';
    protected $allowedFields =['asiakas_id', 'tila'];

    public function tallenna($asiakas, $ostoskori) {

        // Aloita transaktio
        $this->db->transStart();

        // tallennetaan asiakkaan tiedot
        $asiakas_id = $this->tallennaAsiakas($asiakas);

        // tallennetaan tilauksen tiedot
        $tilaus_id = $this->tallennaTilaus($asiakas_id);

        // tallennetaan tilausrivit
        $this->tallennaTilausrivit($tilaus_id, $ostoskori);

        //commit transaktio
        $this->db->transComplete();
        
    }

    private function tallennaAsiakas($asiakas) {
        // Luodaan model
        $asiakasModel = new AsiakasModel();

        $asiakasModel->save($asiakas);
        
        return $this->insertID();
    }

    private function tallennaTilaus($asiakas_id) {
        $this->save([
            'asiakas_id' => $asiakas_id,
            'tila' => 'tilattu'
        ]);

        return $this->insertID();
    }

    private function tallennaTilausrivit($tilaus_id, $ostoskori) {
        $tilausriviModel = new tilausriviModel();
        
        foreach($ostoskori as $tuote_id) {
                $tilausriviModel->save([
                    'tilaus_id' => $tilaus_id,
                    'tuote_id' => $tuote_id,
                    'maara' => 1
        ]); 
        }
    }

    public function tilaus($kayttaja, $ostoskori) {

        // Aloita transaktio
        $this->db->transStart();

        // Päivitetään asiakkaan tiedot
        $asiakas_id = $this->paivitaAsiakas($kayttaja);

        // Tallennetaan tilauksen tiedot
        $tilaus_id = $this->tallennaTilaus($asiakas_id);

        // Tallennetaan tilausrivit
        $this->tallennaTilausrivit($tilaus_id, $ostoskori);

        // Commit transaktio
        $this->db->transComplete();
    }

    private function paivitaAsiakas($kayttaja) {
        // Luodaan model
        $asiakasModel = new AsiakasTietoMuokkausModel();
        
        $id = $asiakasModel->paivita($kayttaja);
        $asiakas_id = $id[0]->id;
        return $asiakas_id;
    }
}
?>