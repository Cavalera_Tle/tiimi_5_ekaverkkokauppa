<?php namespace App\Models;

use CodeIgniter\Model;

class WishlistModel extends Model {

    public function haeKaikki($kayttaja_id) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('wishlist');

        // Haetaan tietyn asiakkaan tuotteet
        $builder->where('kayttaja_id',$kayttaja_id);
        $builder->select('tuote_id');

        // Haetaan tiedot taulusta
        $query = $builder->get();
        

        // Palauttaa kyselyn kontrollerille
        return $query->getResult();
    }

    public function lisaaTuote ($kayttaja_id, $tuote_id) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('wishlist');

         $data = ['kayttaja_id' => $kayttaja_id,'tuote_id' => $tuote_id];
    
        // Tallennetaan tiedot kantaan
        $builder->insert($data);
    }

    public function remove($id) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('wishlist');

        $builder->where('tuote_id', $id);
        $builder->delete();
    }
}
?>