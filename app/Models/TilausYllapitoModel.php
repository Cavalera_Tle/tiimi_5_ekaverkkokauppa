<?php namespace App\Models;

use CodeIgniter\Model;

class TilausYllapitoModel extends Model {

    public function haeKaikki (){

        //avataan tietokantayhteys
        $db = db_connect();

        //luodaan query builder
        $builder = $db->table('tilaus');

        //haetaan kaikki teidot tuoteryhma-taulusta
        $query = $builder->get();

        //palauttaa queryn 
        return $query->getResult();
    }

    public function haeTilaus($id) {
        // avataan tietokantayhteys
        $db = db_connect();

        // luodaan query builder
        $builder = $db->table('tilaus');
        
        $builder->where('id', $id);
        // haetaan kaikki tiedot tuoteryhma-taulusta
        $query = $builder->get();

        // palauttaa queryn kontrollerille
        return $query->getResult();
    }

    public function paivitaTila($id, $tila) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('tilaus');

        $data = ['tila' => $tila];

        $builder->where('id', $id);

        $builder->update($data);
    }
}