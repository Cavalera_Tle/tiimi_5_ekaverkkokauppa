<?php namespace App\Models;

use CodeIgniter\Model;

class TuoteModel extends Model {
    protected $table ='tuote';
    protected $primaryKey ='id';
    protected $allowedFields =['nimi', 'hinta', 'kuvaus', 'varastomaara', 'kuva', 'tuoteryhma_id'];
    protected $returnType = 'object';

    public function haeTuotteet($idt) {
        $result = array();
        foreach ($idt as $id) {
            $this->table('tuote');
            $this->select('id, nimi, hinta, kuvaus, varastomaara, kuva, tuoteryhma_id');
            $this->where('id',$id);
            $query = $this->get();

            $product = $query->getRowArray();
            $this->lisaa_tuote_taulukkoon($product,$result);

            $this->resetQuery();
        }
        return $result;
    }

    private function lisaa_tuote_taulukkoon($product,&$array) {
        for ($i = 0;$i < count($array);$i++) {
            if ($array[$i]['id'] === $product['id']) {
                $array[$i]['maara'] = $array[$i]['maara'] + 1;
                return;
            }
        }
        $product['maara'] = 1;
        array_push($array,$product);
    }
}

?>