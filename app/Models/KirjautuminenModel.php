<?php

namespace App\Models;

use CodeIgniter\Model;

class KirjautuminenModel extends Model
{
    protected $table = 'kayttaja';

    protected $allowedFields = ['kayttaja', 'salasana', 'etunimi', 'sukunimi', 'lahiosoite', 'postinumero', 'postitoimipaikka', 'email', 'puhelin', 'uutiskirje', 'tietojenluovutus', 'rooli'];

    public function check($kayttaja, $salasana) {
            $this->where('rooli', 'asiakas');
            $this->where('kayttaja', $kayttaja);
            $query = $this->get();
           
            $row = $query->getRow();
            if ($row) {
                if (password_verify($salasana, $row->salasana)) {
                    return $row;
                }
                return null;
            }
        }
    }
?>
