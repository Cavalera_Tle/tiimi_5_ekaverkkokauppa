<?php namespace App\Models;

use CodeIgniter\Model;

class PalauteModel extends Model {
    protected $table = 'palaute';

    protected $allowedFields = ['nimi','sposti','otsikko','kuvaus'];

}