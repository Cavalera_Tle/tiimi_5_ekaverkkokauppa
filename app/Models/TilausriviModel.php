<?php namespace App\Models;

use CodeIgniter\Model;

class TilausriviModel extends Model {
    protected $table ='tilausrivi';

    protected $allowedFields =['tilaus_id','tuote_id', 'maara'];

    public function haeKaikki() {
        // Luodaan query builder
        $builder = $this->table('kayttaja');  
        
        // haetaan kaikki tiedot asiakas-taulusta
        $query = $builder->get();

        // palauttaa queryn kontrollerille
        return $query->getResult();
    }   
    
}

?>