<?php namespace App\Models;

use CodeIgniter\Model;

class TuoteYllapitoModel extends Model {

    public function haeKaikki() {
        // avataan tietokantayhteys
        $db = db_connect();

        // luodaan query builder
        $builder = $db->table('tuote');

        // järjestetään id:n mukaan laskevaan järjestykseen
        $builder->orderBy('id', 'ASC');

        // haetaan kaikki tiedot tuote-taulusta
        $query = $builder->get();

        // palauttaa queryn kontrollerille
        return $query->getResult();
    }

    public function lisaaTuote($tuoteNimi, $hinta, $kuvaus, $varastomaara, $kuva, $tuoteryhma_id) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('tuote');

        $data = ['nimi' => $tuoteNimi,
        'hinta' => $hinta,
        'kuvaus' => $kuvaus,
        'varastomaara' => $varastomaara,
        'kuva' => $kuva,
        'tuoteryhma_id' => $tuoteryhma_id
        ];

        //tallennetaan tiedot kantaan
        $builder->insert($data);
    }

    public function remove($id) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('tuote');
        $builder->where('id', $id);
        $builder->delete();
    }

    public function paivitaTuote($id, $tuoteNimi, $hinta, $kuvaus, $varastomaara, $kuva, $tuoteryhma_id) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('tuote');

        $data = ['nimi' => $tuoteNimi,
        'hinta' => $hinta,
        'kuvaus' => $kuvaus,
        'varastomaara' => $varastomaara,
        'kuva' => $kuva,
        'tuoteryhma_id' => $tuoteryhma_id
        ];

        $builder->where('id', $id);
        $builder->update($data);
    }
}
?>