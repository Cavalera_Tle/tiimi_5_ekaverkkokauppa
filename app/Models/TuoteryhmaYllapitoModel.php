<?php namespace App\Models;

use CodeIgniter\Model;

class TuoteryhmaYllapitoModel extends Model {

    public function haeKaikki() {
        // avataan tietokantayhteys
        $db = db_connect();

        // luodaan query builder
        $builder = $db->table('tuoteryhma');

        // järjestetään id:n mukaan laskevaan järjestykseen
        $builder->orderBy('id', 'ASC');

        // haetaan kaikki tiedot tuoteryhma-taulusta
        $query = $builder->get();

        // palauttaa queryn kontrollerille
        return $query->getResult();
    }

    public function haeTuoteryhma($id) {
        // avataan tietokantayhteys
        $db = db_connect();

        // luodaan query builder
        $builder = $db->table('tuoteryhma');
        
        $builder->where('id', $id);
        // haetaan tiedot tuoteryhma-taulusta
        $query = $builder->get();

        // palauttaa queryn kontrollerille
        return $query->getResult();
    }

    public function lisaaTuoteryhma($tuoteryhmanNimi) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('tuoteryhma');

        $data = ['nimi' => $tuoteryhmanNimi];

        //tallennetaan nimi kantaan
        $builder->insert($data);
    }

    public function remove($id) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('tuoteryhma');
        $builder->where('id', $id);
        $builder->delete();
    }

    public function paivitaTuoteryhma($id, $tuoteryhmanNimi) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('tuoteryhma');

        $data= ['nimi' => $tuoteryhmanNimi];

        $builder->where('id', $id);

        $builder->update($data);
    }
}
?>