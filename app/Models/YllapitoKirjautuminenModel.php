<?php

namespace App\Models;

use CodeIgniter\Model;

class YllapitoKirjautuminenModel extends Model {
    protected $table = 'kayttaja';
    protected $allowedFields = ['kayttaja', 'salasana', 'etunimi', 'sukunimi', 'lahiosoite', 'postinumero', 'postitoimipaikka', 'email', 'puhelin', 'uutiskirje', 'tietojenluovutus', 'rooli'];

    public function check($kayttaja, $salasana) {
        $this->where('rooli', 'yllapitaja');
        $this->where('kayttaja', $kayttaja);
        $query = $this->get();
        // print $this->getLastQuery();
        $row = $query->getRow();
        if ($row) {
            if (password_verify($salasana, $row->salasana)) {
                return $row;
            } else {
                return null;
            }
        }
    }
}
?>

