<?php namespace App\Models;

use CodeIgniter\Model;

class AsiakasTietoMuokkausModel extends Model {

    public function haeTiedot() {
        // avataan tietokantayhteys
        $db = db_connect();

        // luodaan query builder
        $builder = $db->table('kayttaja');

        // haetaan kaikki tiedot asiakas-taulusta
        $query = $builder->get();

        // palauttaa queryn kontrollerille
        return $query->getResult();
    }

    public function haeAsiakas($asiakas_id) {
        // Avataan tietokantayhteys
        $db = db_connect();

        // Luodaan query builder
        $builder = $db->table('kayttaja');

        // Haetaan tietyn asiakkaan tuotteet
        $builder->where('id',$asiakas_id);
        //$builder->select('tuote_id');

        // Haetaan tiedot taulusta
        $query = $builder->get();
        

        // Palauttaa kyselyn kontrollerille
        return $query->getResult();
    }

    public function paivitaTiedot($id,$kayttaja, $salasana, $etunimi, $sukunimi, $lahiosoite, $postinumero, $postitoimipaikka, $sahkoposti, $puhelin, $uutiskirje, $tietojenluovutus) {
        // avataan tietokantayhteys
        $db = db_connect();

        // luodaan query builder
        $builder = $db->table('kayttaja');

        $data = [
            'kayttaja' => $kayttaja,
            'salasana' => $salasana,
            'etunimi' => $etunimi,
            'sukunimi' => $sukunimi,
            'lahiosoite' => $lahiosoite,
            'postinumero' => $postinumero,
            'postitoimipaikka' => $postitoimipaikka,
            'email' => $sahkoposti,
            'puhelin' => $puhelin,
            'uutiskirje' => $uutiskirje,
            'tietojenluovutus' => $tietojenluovutus        
        ];
        $builder->where('id', $id);
        //tallennetaan tiedot kantaan
        $builder->update($data);
    }

    public function paivita($asiakas) {
        // avataan tietokantayhteys
        $db = db_connect();     
        // luodaan query builder
        $builder = $db->table('asiakas');

        $kayttaja_id = $asiakas['id'];
        //$kayttaja = $asiakas['kayttaja'];
        $etunimi = $asiakas['etunimi'];
        $sukunimi = $asiakas['sukunimi'];
        $lahiosoite = $asiakas['lahiosoite'];
        $postinumero = $asiakas['postinumero'];
        $postitoimipaikka = $asiakas['postitoimipaikka'];
        $email = $asiakas['email'];
        $puhelin = $asiakas['puhelin'];

        $data = [
            'etunimi' => $etunimi,
            'sukunimi' => $sukunimi,
            'lahiosoite' => $lahiosoite,
            'postinumero' => $postinumero,
            'postitoimipaikka' => $postitoimipaikka,
            'email' => $email,
            'puhelin' => $puhelin,
            'kayttaja_id' => $kayttaja_id     
        ];
        
        //tallennetaan tiedot kantaan
        $builder->insert($data);

        $builder->selectMax('id');
        $query = $builder->get();

        // Palauttaa kyselyn kontrollerille
        return $query->getResult();
    }
}
?>